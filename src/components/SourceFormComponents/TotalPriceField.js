import React, { useEffect } from "react";
import { Field } from "formik";
// import { TextField } from "@material-ui/core";
import useStyles from "../hooks/useStyles";
import CurrencyInput from "react-currency-input-field";
import useDebounce from "../hooks/useDebounce";

function TotalPriceField(props) {
	const { name, setFieldValue, feePrice, feeVat } = props;
	// const { values, index, name, setFieldValue, feePrice, feeVat } = props;
	// const { feePrice, feeVat } = values.saleFees[index].fixedFee;
	const debouncedSetFieldValue = useDebounce((name, feePrice, feeVat) => {
		let totalPrice = 0;
		if (!feePrice) {
			totalPrice = 0;
		} else if (feePrice && feeVat) {
			totalPrice = feePrice + feeVat;
		} else {
			totalPrice = feePrice;
		}

		return setFieldValue(name, Number(totalPrice.toFixed(2)));
	}, 200);

	useEffect(() => {
		// let totalPrice = 0;
		// if (!feePrice) {
		// 	totalPrice = 0;
		// } else if (feePrice && feeVat) {
		// 	totalPrice = feePrice + feeVat;
		// } else {
		// 	totalPrice = feePrice;
		// }
		// setFieldValue(name, Number(totalPrice.toFixed(2)));
		debouncedSetFieldValue(name, feePrice, feeVat);
	}, [feePrice, feeVat, name, debouncedSetFieldValue]);

	const classes = useStyles();

	return (
		<Field
			className={classes.MuiFormControlPrice}
			variant="outlined"
			margin="dense"
			name={props.name}
			// as={TextField}
			label="Total Price (£)"
			color="primary"
			type="number"
			required
			// disabled
			value={props.value}
		>
			{({ field, form, meta }) => (
				// <CurrencyTextField />
				<CurrencyInput
					prefix="£ "
					{...field}
					className={classes.MuiOutlinedInputCurrencyInput}
					style={{
						height: "40px",
						borderRadius: "5px",
						padding: "0px 0px 0px 24px",
						margin: "0em 1em 1em 1em",
						width: "13em",
						zIndex: "3",
						fontSize: "medium",
					}}
					// className={`form-control ${classes.MuiFormControlPrice}`}
					decimalsLimit={2}
					decimalScale={2}
					fixedDecimalLength="2"
					disabled={true}
				/>
			)}
		</Field>
	);
}

export default TotalPriceField;
