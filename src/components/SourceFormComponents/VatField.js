import React, { useEffect } from "react";
import { Field } from "formik";
// import { TextField } from "@material-ui/core";
import useStyles from "../hooks/useStyles";
import CurrencyInput from "react-currency-input-field";

// import useDebounce from "../hooks/useDebounce";
// import { useDebouncedCallback } from "use-debounce";
// import CurrencyTextField from "@unicef/material-ui-currency-textfield";
// import NumberFormat from "react-number-format";

// function useDebounce(callback, delay) {
// 	const debouncedCallBack = useCallback(debounce(callback, delay), []);
// 	return debouncedCallBack;
// }

// const INPUT_DELAY = 200;

const CustomInputWrapper = (props) => {
	// value,
	const { name, vatable, feePrice, setFieldValue } = props;
	// const [innerVatValue, setInnerVatValue] = useState(0);

	// const debouncedSetFieldValue = useDebounce((name, vatable, feePrice) => {
	// 	let Vat = 0;
	// 	if (vatable) {
	// 		Vat = Number((feePrice * 0.2).toFixed(2));
	// 		console.log(typeof Vat);
	// 		console.log(Vat);
	// 	} else {
	// 		Vat = Number((0).toFixed(2));
	// 	}

	// 	setInnerVatValue(Vat);
	// 	setFieldValue(name, innerVatValue);

	// 	// return setFieldValue(name, Vat);
	// }, 200);

	useEffect(() => {
		let Vat = 0;
		if (vatable) {
			Vat = Number((feePrice * 0.2).toFixed(2));
		} else {
			Vat = Number((0).toFixed(2));
		}
		// console.log(Vat);
		// console.log(vatable);
		// debouncedSetFieldValue(name, vatable, feePrice);
		setFieldValue(name, Vat);
	}, [name, vatable, feePrice, setFieldValue]);
	//
	const classes = useStyles();
	return (
		<CurrencyInput
			prefix="£ "
			// {...props}
			// className={`form-control ${classes.MuiFormControlPrice}`}
			className={classes.MuiOutlinedInputCurrencyInput}
			decimalsLimit={2}
			style={{
				height: "40px",
				borderRadius: "5px",
				padding: "0px 0px 0px 24px",
				margin: "0em 1em 1em 1em",
				// padding: "0px",
				// height: "40px",
				width: "13em",
				zIndex: "3",
				fontSize: "medium",
			}}
			decimalScale={2}
			fixedDecimalLength="2"
			disabled={true}
			value={props.value}
			// value={props.value}
			// value={innerVatValue}
			// onChange={handleOnChange}
		/>
	);
};

export default function VatField(props) {
	// handleChange
	const { name, setFieldValue, value, feePrice, vatable, feeOriginType } = props;
	// const { values, index, name, setFieldValue, value, feeType, feePrice, vatable } = props;
	// const { feePrice } = values.saleFees[index].fixedFee;
	// const { vatable } = values.saleFees[index];
	// console.log(vatable);
	// console.log(feePrice);
	// console.log(value);
	// console.log(feeOriginType);

	// const debouncedSetFieldValue = useDebounce((name, Vat) => setFieldValue(name, Vat, false), 200);
	// const debouncedSetFieldValue = useDebounce((name, vatable, feePrice) => {
	// 	let Vat = 0;
	// 	if (vatable) {
	// 		Vat = Number((feePrice * 0.2).toFixed(2));
	// 		// console.log(typeof Vat);
	// 	} else {
	// 		Vat = Number((0).toFixed(2));
	// 	}

	// 	return setFieldValue(name, Vat);
	// }, 1000);

	// useEffect(() => {
	// 	let Vat = 0;
	// 	if (feeOriginType === "custom") {
	// 		if (vatable) {
	// 			Vat = Number((feePrice * 0.2).toFixed(2));
	// 			// console.log(typeof Vat);
	// 		} else {
	// 			Vat = Number((0).toFixed(2));
	// 		}
	// 		setInnerVatValue(Vat);
	// 		setFieldValue(name, innerVatValue);
	// 	} else if (feeOriginType === "standard") {
	// 		debouncedSetFieldValue(name, vatable, feePrice);
	// 	}
	// }, [name, vatable, debouncedSetFieldValue]);
	// // [feePrice, setFieldValue, name, vatable];

	const classes = useStyles();

	return (
		<Field
			className={classes.MuiFormControlPrice}
			variant="outlined"
			margin="dense"
			name={name}
			// as={TextField}
			label="Vat 20% (£)"
			color="primary"
			type="number"
			required
			disabled
			value={value}
		>
			{({ field, form, meta }) => (
				<CustomInputWrapper
					name={name}
					// onChange={handleChange}
					value={value}
					vatable={vatable}
					feeOriginType={feeOriginType}
					feePrice={feePrice}
					setFieldValue={setFieldValue}
				/>
			)}
		</Field>
	);
}
