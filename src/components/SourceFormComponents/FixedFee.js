import React from "react";
import { ErrorMessage, FastField } from "formik";

import { Grid } from "@material-ui/core";
// import InputBase from "@material-ui/core/InputBase";
import InputLabel from "@material-ui/core/InputLabel";
import TextField from "@material-ui/core/TextField";
// import FormControl from "@material-ui/core/FormControl";
// import { alpha, ThemeProvider, withStyles, makeStyles, createTheme } from "@material-ui/core/styles";
import InputAdornment from "@material-ui/core/InputAdornment";
import useStyles from "../hooks/useStyles";
// import NetPriceField from "./NetPriceField";
import VatField from "./VatField";
import TotalPriceField from "./TotalPriceField";
// import CurrencyInput from "react-currency-input-field";

function FixedFee(props) {
	const classes = useStyles();
	//  values,
	// , touchedFixedFee
	// , handleChange
	const { setFieldValue, vatable, index, fee, feeCategoryName, disabled } = props;
	const priceName = `${feeCategoryName}[${index}].fixedFee.feePrice`;
	const priceValue = fee.fixedFee.feePrice;
	const vatName = `${feeCategoryName}[${index}].fixedFee.feeVat`;
	const vatValue = fee.fixedFee.feeVat;
	const totalPriceName = `${feeCategoryName}[${index}].fixedFee.feeTotalPrice`;
	const totalPriceValue = fee.fixedFee.feeTotalPrice;
	const feeOriginType = fee.feeOriginType;
	// console.log(touchedFixedFee);
	// console.log(fee);
	// console.log(fee.feeOriginType);
	return (
		<Grid container item>
			<Grid item xs={11} sm="auto">
				{/* <InputLabel shrink style={{ margin: "0em 0em 0em 1.5em", fontSize: "medium" }}>
					Price (Net of VAT) (£)
				</InputLabel> */}
				<FastField
					className={classes.MuiFormControlPrice}
					variant="outlined"
					margin="dense"
					name={priceName}
					as={TextField}
					label="Price (Net of VAT) (£)"
					color="primary"
					type="number"
					required
					// min="0"
					// min="0"
					InputProps={{
						startAdornment: <InputAdornment position="start">£</InputAdornment>,
					}}
					disabled={disabled}
				/>
				{/* {() => (
						<CurrencyInput
							prefix="£ "
							decimalsLimit={2}
							// decimalScale={2}
							fixedDecimalLength="2"
							style={{
								height: "40px",
								borderRadius: "5px",
								padding: "0px 0px 0px 24px",
								margin: "0em 1em 0em 1em",
								// border: "0.5px solid #4A90E2",
								// padding: "0px",
								// height: "40px",
								width: "11em",
								zIndex: "3",
								fontSize: "medium",
							}}
						/>
					)} */}
				{/* </Field> */}

				{/* {((priceValue <= 0 && touchedFixedFee.feePrice) || (!priceValue && touchedFixedFee.feePrice)) && ( */}
				{(priceValue <= 0 || !priceValue) && (
					<ErrorMessage
						name={priceName}
						render={(msg) => <div className={classes.error}>{msg}</div>}
					/>
				)}
			</Grid>
			<Grid item xs={11} sm="auto">
				<InputLabel shrink style={{ margin: "0em 0em 0em 1.5em", fontSize: "medium" }}>
					VAT 20% (£)
				</InputLabel>
				<VatField
					name={vatName}
					value={vatValue}
					// values={values}
					setFieldValue={setFieldValue}
					// index={index}
					// feeType={feeType}
					feePrice={priceValue}
					vatable={vatable}
					feeOriginType={feeOriginType}
					// handleChange={handleChange}
				/>
			</Grid>
			<Grid item xs={11} sm="auto">
				<InputLabel shrink style={{ margin: "0em 0em 0em 1.5em", fontSize: "medium" }}>
					Total Price (£)
				</InputLabel>
				<TotalPriceField
					name={totalPriceName}
					value={totalPriceValue}
					// values={values}
					setFieldValue={setFieldValue}
					// index={index}
					// feeType={feeType}
					feePrice={priceValue}
					feeVat={vatValue}
					// handleChange={handleChange}
				/>
			</Grid>
		</Grid>
	);
}

export default React.memo(FixedFee);
