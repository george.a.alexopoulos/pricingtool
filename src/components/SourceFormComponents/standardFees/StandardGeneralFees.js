import { v4 as uuidv4 } from "uuid";

const StandardGeneralFees = [
	{
		fee_id: uuidv4(),
		feeName: "ID/AML Electronic Searches",
		feeDescription: "ID/AML Electronic Search Fees",
		vatable: true,
		feeOriginType: "standard",
		feeType: "fixed",
		fixedFee: {
			feePrice: 20.0,
			feeVat: 4.0,
			feeTotalPrice: 24.0,
		},
		variableFee: [],
	},
	{
		fee_id: uuidv4(),
		feeName: "Same Day Bank Transfer (Conveyancing)",
		feeDescription: "Same Day Electronic Bank Transfer",
		vatable: true,
		feeOriginType: "standard",
		feeType: "fixed",
		fixedFee: {
			feePrice: 30.0,
			feeVat: 6.0,
			feeTotalPrice: 36.0,
		},
		variableFee: [],
	},
	{
		fee_id: uuidv4(),
		feeName: "Lender Portal Fee",
		feeDescription: "",
		vatable: true,
		feeOriginType: "standard",
		feeType: "fixed",
		fixedFee: {
			feePrice: 25.0,
			feeVat: 5.0,
			feeTotalPrice: 30.0,
		},
		variableFee: [],
	},
];

export default StandardGeneralFees;
