import { v4 as uuidv4 } from "uuid";

const StandardRemortgageTOEFees = [
	{
		fee_id: uuidv4(),
		feeName: "Remortgage Conveyancing Fees",
		feeDescription: "Standard",
		vatable: true,
		feeOriginType: "standard",
		feeType: "fixed",
		fixedFee: {
			feePrice: 350.0,
			feeVat: 70.0,
			feeTotalPrice: 420.0,
		},
		variableFee: [],
	},
	{
		fee_id: uuidv4(),
		feeName: "Remortgage and Title Transfer",
		feeDescription: "",
		vatable: true,
		feeOriginType: "standard",
		feeType: "fixed",
		fixedFee: {
			feePrice: 600.0,
			feeVat: 120.0,
			feeTotalPrice: 720.0,
		},
		variableFee: [],
	},
	{
		fee_id: uuidv4(),
		feeName: "Lifetime/Shared Equity Mortgage",
		feeDescription: "Bespoke fee so fee quoted here is approximate. Discuss billing with a Director on a case by case basis.",
		vatable: true,
		feeOriginType: "standard",
		feeType: "fixed",
		fixedFee: {
			feePrice: 600.0,
			feeVat: 120.0,
			feeTotalPrice: 720.0,
		},
		variableFee: [],
	},
	{
		fee_id: uuidv4(),
		feeName: "Discharge of Standard Security",
		feeDescription: "For example, mortgage paid-off but security remains in place.",
		vatable: true,
		feeOriginType: "standard",
		feeType: "fixed",
		fixedFee: {
			feePrice: 200.0,
			feeVat: 40.0,
			feeTotalPrice: 240.0,
		},
		variableFee: [],
	},
];

export default StandardRemortgageTOEFees;
