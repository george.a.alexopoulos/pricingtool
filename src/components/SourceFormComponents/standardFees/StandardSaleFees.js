import { v4 as uuidv4 } from "uuid";

const StandardSaleFees = [
	{
		fee_id: uuidv4(),
		feeName: "Conveyancing Fees Sale",
		feeDescription: "",
		vatable: true,
		feeOriginType: "standard",
		feeType: "variable",
		fixedFee: "",
		variableFee: [
			{
				scaleMin: 0,
				scaleMax: 200000.0,
				feePrice: 600.0,
				feeVat: 120.0,
				feeTotalPrice: 720.0,
			},
			{
				scaleMin: 200000.1,
				scaleMax: 400000.0,
				feePrice: 750.0,
				feeVat: 150.0,
				feeTotalPrice: 900.0,
			},
			{
				scaleMin: 400000.1,
				scaleMax: 600000.0,
				feePrice: 900.0,
				feeVat: 180.0,
				feeTotalPrice: 1080.0,
			},
			{
				scaleMin: 600000.1,
				scaleMax: 1000000000000,
				feePrice: 1100.0,
				feeVat: 220.0,
				feeTotalPrice: 1320.0,
			},
		],
	},
];

export default StandardSaleFees;
