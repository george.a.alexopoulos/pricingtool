import { v4 as uuidv4 } from "uuid";

const ReferalFees = [
	{
		fee_id: uuidv4(),
		feeName: "Referral Fee",
		feeDescription: "",
		vatable: false,
		feeOriginType: "standard",
		feeType: "fixed",
		fixedFee: {
			feePrice: 0.0,
			feeVat: 0.0,
			feeTotalPrice: 0.0,
		},
		variableFee: [],
	},
];

export default ReferalFees;
