import React from "react";
import { ErrorMessage, FieldArray, FastField } from "formik";
import { Grid } from "@material-ui/core";
// import { Typography } from "@material-ui/core";
import { Button } from "@material-ui/core";
import { Tooltip } from "@material-ui/core";
import { IconButton } from "@material-ui/core";
import { Divider } from "@material-ui/core";
// import InputBase from "@material-ui/core/InputBase";
import InputLabel from "@material-ui/core/InputLabel";
import TextField from "@material-ui/core/TextField";
// import FormControl from "@material-ui/core/FormControl";
// import { alpha, ThemeProvider, withStyles, makeStyles, createTheme } from "@material-ui/core/styles";
import InputAdornment from "@material-ui/core/InputAdornment";
import useStyles from "../hooks/useStyles";
// import NetPriceField from "./NetPriceField";
import VatField from "./VatField";
import TotalPriceField from "./TotalPriceField";
// import { TiDocumentAdd } from "react-icons/ti";
// import { TiDelete } from "react-icons/ti";
import { MdOutlineAddCircle } from "react-icons/md";
// import { MdOutlineAddCircleOutline } from "react-icons/md";
import { MdDangerous } from "react-icons/md";
// import { MdOutlineDangerous } from "react-icons/md";

import { withStyles } from "@material-ui/core/styles";
// import CurrencyInput from "react-currency-input-field";
// import { v4 as uuidv4 } from "uuid";

const variableEmptyFee = {
	scaleMin: "",
	scaleMax: "",
	feePrice: "",
	feeVat: 0.0,
	feeTotalPrice: 0.0,
};

const AddTooltip = withStyles((theme) => ({
	arrow: {
		color: "blue",
	},
	tooltip: {
		// backgroundColor: theme.palette.common.white,
		backgroundColor: "blue",
		color: "theme.palette.common.white",
		boxShadow: theme.shadows[1],
		fontSize: 11,
	},
}))(Tooltip);

const DeleteTooltip = withStyles((theme) => ({
	arrow: {
		color: "red",
	},
	tooltip: {
		// backgroundColor: theme.palette.common.white,
		backgroundColor: "red",
		color: "theme.palette.common.white",
		boxShadow: theme.shadows[1],
		fontSize: 11,
	},
}))(Tooltip);

function VariableFee(props) {
	const classes = useStyles();
	// const { priceName, priceValue, vatName, vatValue, setFieldValue, totalPriceName, totalPriceValue, vatable, values, form, index, feeCategory } =
	// 	props;
	// values,
	// touchedVariableFee
	// , handleChange
	const { setFieldValue, vatable, index, fee, feeCategoryName, disabled } = props;
	// const variableFee = fee.variableFee;
	// console.log(variableFee);
	// const variableFeeCategory = values.feeCategoryName;
	// console.log(variableFeeCategory);
	// console.log(fee);
	// console.log(index);
	// console.log(touchedVariableFee);
	// const { handleChange } = formProps;
	// console.log(formProps);
	// console.log("fee price is:", typeof values.saleFees[0].feePrice);
	// console.log("vat value is: ", typeof vatValue);
	// console.log(vatValue);
	// console.log(values);
	// console.log(values.saleFess[index].vatable);
	// console.log(priceName);
	// console.log(`${priceName[0]}.feePrice`);
	// console.log(priceName[0].feePrice);
	// console.log(`${values.saleFees}[${index}].variableFee[0].feePrice`);
	// console.log(`${feeCategoryName}[${index}].variableFee[0].feePrice`);

	// console.log(vatName);
	// console.log(vatValue);
	// console.log(priceValue);
	// console.log(typeof priceValue);
	return (
		<Grid container item>
			{/* <Grid item xs={11} sm="auto" style={{ margin: "0em 0em 1em 0em" }}>
				<Typography>Variable Fees Under construction</Typography>
			</Grid> */}
			<FieldArray name={`${feeCategoryName}[${index}].variableFee`}>
				{(props) => {
					// console.log("Variable FieldArray props: ", props);
					// console.log("parent form values: ", values);
					// console.log("Values from Variable fee", values[`${feeCategoryName}`].variableFee);
					// console.log("Values from Variable fee", fee.variableFee);
					// console.log(fee);
					// console.log(fee.variableFee);
					const { push, remove } = props;
					return (
						<>
							{fee.variableFee.map((_, variableFeeIndex) => (
								<Grid
									container
									direction="column"
									spacing={2}
									key={variableFeeIndex}
								>
									<Grid container direction="row" item>
										<Grid item xs={11} sm="auto">
											<FastField
												className={classes.MuiFormControlPrice}
												variant="outlined"
												margin="dense"
												name={`${feeCategoryName}[${index}].variableFee[${variableFeeIndex}].scaleMin`}
												as={TextField}
												label="Scale value min"
												color="primary"
												type="number"
												required
												InputProps={{
													startAdornment: (
														<InputAdornment position="start">
															£
														</InputAdornment>
													),
												}}
												disabled={disabled}
											/>
											<ErrorMessage
												name={`${feeCategoryName}[${index}].variableFee[${variableFeeIndex}].scaleMin`}
												render={(msg) => (
													<div className={classes.error}>{msg}</div>
												)}
											/>
										</Grid>
										<Grid item xs={11} sm="auto">
											<FastField
												className={classes.MuiFormControlPrice}
												variant="outlined"
												margin="dense"
												name={`${feeCategoryName}[${index}].variableFee[${variableFeeIndex}].scaleMax`}
												as={TextField}
												label="Scale value max"
												color="primary"
												type="number"
												required
												InputProps={{
													startAdornment: (
														<InputAdornment position="start">
															£
														</InputAdornment>
													),
												}}
												disabled={disabled}
											/>
											<ErrorMessage
												name={`${feeCategoryName}[${index}].variableFee[${variableFeeIndex}].scaleMax`}
												render={(msg) => (
													<div className={classes.error}>{msg}</div>
												)}
											/>
										</Grid>
									</Grid>
									<Grid container item key={variableFeeIndex}>
										<Grid item xs={11} sm="auto">
											<FastField
												className={classes.MuiFormControlPrice}
												variant="outlined"
												margin="dense"
												name={`${feeCategoryName}[${index}].variableFee[${variableFeeIndex}].feePrice`}
												as={TextField}
												label="Price (Net of VAT) (£)"
												color="primary"
												type="number"
												required
												InputProps={{
													startAdornment: (
														<InputAdornment position="start">
															£
														</InputAdornment>
													),
												}}
												disabled={disabled}
											/>
											{(fee.variableFee[variableFeeIndex].feePrice <= 0 ||
												!fee.variableFee[variableFeeIndex].feePrice) && (
												<ErrorMessage
													name={`${feeCategoryName}[${index}].variableFee[${variableFeeIndex}].feePrice`}
													render={(msg) => (
														<div className={classes.error}>{msg}</div>
													)}
												/>
											)}
										</Grid>
										<Grid item xs={11} sm="auto">
											<InputLabel
												shrink
												style={{
													margin: "0em 0em 0em 1.5em",
													fontSize: "medium",
												}}
											>
												VAT 20% (£)
											</InputLabel>
											<VatField
												name={`${feeCategoryName}[${index}].variableFee[${variableFeeIndex}].feeVat`}
												value={fee.variableFee[variableFeeIndex].feeVat}
												setFieldValue={setFieldValue}
												// index={index}
												// feeType={feeType}
												feePrice={
													fee.variableFee[variableFeeIndex].feePrice
												}
												vatable={vatable}
												// handleChange={handleChange}
											/>
										</Grid>
										<Grid item xs={11} sm="auto">
											<InputLabel
												shrink
												style={{
													margin: "0em 0em 0em 1.5em",
													fontSize: "medium",
												}}
											>
												Total Price (£)
											</InputLabel>
											<TotalPriceField
												name={`${feeCategoryName}[${index}].variableFee[${variableFeeIndex}].feeTotalPrice`}
												value={
													fee.variableFee[variableFeeIndex].feeTotalPrice
												}
												setFieldValue={setFieldValue}
												// index={index}
												// feeType={feeType}
												feePrice={
													fee.variableFee[variableFeeIndex].feePrice
												}
												feeVat={fee.variableFee[variableFeeIndex].feeVat}
												// handleChange={handleChange}
											/>
										</Grid>
										<Grid item>
											{!disabled && (
												<Button
													variant="contained"
													color="primary"
													style={{
														minWidth: "11em",
														height: "40px",
														margin: "1em 0em 0em 1em",
													}}
													onClick={() => {
														push(variableEmptyFee);
														console.log(
															"i will add a new scale for this variable fee"
														);
													}}
												>
													{fee.variableFee.length === 0
														? "Add Scale"
														: "Add Another Scale"}
												</Button>
											)}
										</Grid>
										<Grid item>
											<Grid
												container
												direction="row"
												spacing={7}
												style={{ padding: "0.7em" }}
											>
												<Grid item style={{ position: "relative" }}>
													{!disabled && (
														<AddTooltip
															style={{
																position: "absolute",
																// top: "0.25em",
																// right: "0em",
																cursor: "pointer",
															}}
															arrow
															title={
																fee.variableFee.length === 0
																	? "Add Scale"
																	: "Add Another Scale"
															}
															placement="bottom"
															enterDelay={100}
															leaveDelay={150}
														>
															<IconButton
																aria-label="add"
																onClick={() => {
																	push(variableEmptyFee);
																	// console.log("i will add a new scale for this variable fee");
																}}
															>
																<MdOutlineAddCircle
																	size={28}
																	color="blue"
																/>
															</IconButton>
														</AddTooltip>
													)}
												</Grid>
												{!disabled &&
													fee.variableFee.length > 1 &&
													variableFeeIndex > 0 && (
														<Grid item style={{ position: "relative" }}>
															<DeleteTooltip
																style={{
																	position: "absolute",
																	// top: "0.25em",
																	// right: "10px",
																	cursor: "pointer",
																}}
																arrow
																title="Delete Scale"
																placement="bottom"
																enterDelay={100}
																leaveDelay={150}
															>
																<IconButton
																	aria-label="delete"
																	onClick={() => {
																		remove(variableFeeIndex);
																		console.log(
																			"i will add a new scale for this variable fee"
																		);
																	}}
																>
																	<MdDangerous
																		size={28}
																		color="red"
																	/>
																</IconButton>
															</DeleteTooltip>
														</Grid>
													)}
											</Grid>
										</Grid>
									</Grid>
									<Divider
										style={{ width: "100%", margin: "0rem 0rem 2rem 0rem" }}
									/>
								</Grid>
							))}
						</>
					);
				}}
			</FieldArray>
		</Grid>
	);
}

export default React.memo(VariableFee);
