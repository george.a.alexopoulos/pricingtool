import React, { useState, useCallback } from "react";

import {
	// Card,
	// CardContent,
	Grid,
	Typography,
	// TextField,
	// MenuItem,
	Button,
	// CircularProgress,
	Divider,
	Radio,
	RadioGroup,
	FormControlLabel,
	FormControl,
	FormLabel,
	Tooltip,
	IconButton,
	// Switch,
} from "@material-ui/core";
import useStyles from "../hooks/useStyles";
import useToggle from "../hooks/useToggle";
import Accordion from "@material-ui/core/Accordion";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionActions from "@material-ui/core/AccordionActions";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import { MdDone } from "react-icons/md";
import { MdEdit } from "react-icons/md";
import { withStyles } from "@material-ui/core/styles";
import FeeFieldArray from "./FeeFieldArray";
import { v4 as uuidv4 } from "uuid";

const emptyFee = {
	fee_id: uuidv4(),
	feeName: "",
	feeDescription: "",
	vatable: true,
	feeOriginType: "custom",
	feeType: "",
	fixedFee: "",
	variableFee: [],
};

const EditTooltip = withStyles((theme) => ({
	arrow: {
		color: "blue",
	},
	tooltip: {
		// backgroundColor: theme.palette.common.white,
		backgroundColor: "blue",
		color: "theme.palette.common.white",
		boxShadow: theme.shadows[1],
		fontSize: 11,
	},
}))(Tooltip);

function FeeAccordion(props) {
	const {
		// expanded,
		feeName,
		// formik,
		// isCompleted,
		// open,
		// toggleOpen,
		// toggleIsComplete,
		feeCategoryName,
		values,
		// length,
		setFieldValue,
		// isValid,
		StandardFees,
		errors,
		// handleChange,
	} = props;

	// console.log(values.sourceName);
	// console.log(errors);
	// console.log(errors[feeCategoryName]);
	// console.log(feeCategoryName);

	// console.log(values.sourceName);
	// console.log(formik);
	// console.log(formik.isValid);
	// console.log(isValid);
	// console.log(length);

	// console.log(values[feeCategoryName].length);
	// console.log(values);

	const [isOpen, toggleIsOpen] = useToggle(true);
	const [isCompleted, toggleIsCompleted] = useToggle(false);
	const [showAddFeeButtom, setShowAddFeeButtom] = useState(false);
	const [feeOrigin, setFeeOrigin] = useState("");
	// const [openPurchases, toggleOpenPurchases] = useToggle(true);
	// const [isCompletePurchases, toggleIsCompletePurchases] = useToggle(false);
	const [isDisabledRadio, toggleIsDisabledRadio] = useToggle(false);
	const classes = useStyles();

	// useEffect(() => {
	// 	console.log("isOpen : ", isOpen);
	// 	console.log("isCompleted", isCompleted);
	// }, [isOpen, isCompleted]);

	// const handleStart = useCallback(
	// 	(e) => {
	// 		// console.log(values);
	// 		// console.log(name);
	// 		if (e.target.value === "standard") {
	// 			setFieldValue(feeCategoryName, StandardFees);
	// 			toggleIsOpen();
	// 			toggleIsCompleted();
	// 			toggleIsDisabledRadio();
	// 		}
	// 		if (e.target.value === "custom") {
	// 			setFieldValue(feeCategoryName, []);
	// 			setShowAddFeeButtom(true);
	// 		}
	// 		// console.log("I have selected a way to start with", e.target.value);
	// 	},
	// 	[setFieldValue, feeCategoryName, StandardFees, toggleIsOpen, toggleIsCompleted, toggleIsDisabledRadio]
	// );

	const handleStart = useCallback(
		(e) => {
			// console.log(values);
			// console.log(name);
			if (e.target.value === "custom") {
				setFeeOrigin("custom");
				setFieldValue(feeCategoryName, [emptyFee]);
				// setFieldValue(feeCategoryName, []);
				setShowAddFeeButtom(true);
			}
			if (e.target.value === "standard") {
				setFeeOrigin("standard");
				setFieldValue(feeCategoryName, StandardFees);
				toggleIsOpen();
				toggleIsCompleted();
				toggleIsDisabledRadio();
			}
			// console.log("I have selected a way to start with", e.target.value);
		},
		[
			setFieldValue,
			feeCategoryName,
			StandardFees,
			toggleIsOpen,
			toggleIsCompleted,
			toggleIsDisabledRadio,
		]
	);

	// useEffect(() => {
	// 	console.log(values);
	// }, [values]);

	return (
		values.sourceName && (
			<Accordion expanded={isOpen}>
				<AccordionSummary
					expandIcon={<ExpandMoreIcon />}
					aria-controls="panel1c-content"
					id="panel1c-header"
				>
					<Grid container direction="column" spacing={4}>
						<Grid
							container
							direction="row"
							style={{ position: "relative" }}
							alignItems="center"
						>
							<Grid item>
								<Typography
									variant="h5"
									style={{ margin: "0em 0em 0em", padding: "1em 0em 1em 0.5em" }}
								>
									{`${feeName} Fees`}
								</Typography>
							</Grid>
							<Grid
								item
								xs={11}
								sm="auto"
								style={{ marginLeft: "5em", padding: "1em 0em 0.5em 0em" }}
							>
								<FormControl component="fieldset">
									<FormLabel component="legend" style={{ fontSize: "100%" }}>
										Select how to start:
									</FormLabel>
									{/* name={`${feeCategoryName}[${index}].feeType`} value={_.feeType} */}
									{/* style={{ fontSize: "small" }} */}
									<RadioGroup
										onChange={handleStart}
										style={{ margin: "0em 0em 0em", padding: "0em 1em 0em" }}
									>
										<FormControlLabel
											value={"standard"}
											control={<Radio color="primary" />}
											label="Apply Standard Fees"
											className={classes.xsmallRadioButton}
											disabled={isDisabledRadio}
										/>
										<FormControlLabel
											value={"custom"}
											control={<Radio />}
											label="Create Custom Fees"
											className={classes.xsmallRadioButton}
											style={{ marginTop: "-10px" }}
											disabled={isDisabledRadio}
										/>
									</RadioGroup>
								</FormControl>
							</Grid>

							{!errors[feeCategoryName] && isCompleted && (
								<Grid item>
									<Typography
										variant="h5"
										style={{
											margin: "1.5em 0em 0em",
											color: "#4caf50",
											fontStyle: "italic",
											position: "absolute",
											top: "0em",
											bottom: "0em",
											right: "3em",
										}}
									>
										Completed <MdDone size={25} color="green" />
									</Typography>
								</Grid>
							)}
							{!isOpen && (
								<EditTooltip
									style={{
										position: "absolute",
										top: "0em",
										bottom: "0em",
										right: "0em",
										cursor: "pointer",
									}}
									arrow
									title="Edit Fee"
									placement="bottom"
									enterDelay={100}
									leaveDelay={150}
									onClick={() => {
										toggleIsOpen();
										toggleIsCompleted();
										toggleIsDisabledRadio();
									}}
								>
									<IconButton aria-label="edit">
										<MdEdit size={25} color="blue" />
									</IconButton>
								</EditTooltip>
							)}
						</Grid>
						{/* place the radio here if needed  to place it under the TypoGrapghy*/}
					</Grid>
				</AccordionSummary>
				<Divider style={{ width: "100%", margin: "0em 0em 1em 0em" }} />
				<AccordionDetails>
					<FeeFieldArray
						feeName={feeName}
						feeCategoryName={feeCategoryName}
						showAddFeeButtom={showAddFeeButtom}
						emptyFee={emptyFee}
						feeOrigin={feeOrigin}
					/>
				</AccordionDetails>
				<AccordionActions>
					{values[feeCategoryName].length > 0 && !errors[feeCategoryName] && (
						<Grid item>
							<Button
								variant="contained"
								color="primary"
								style={{
									minWidth: "11em",
									height: "40px",
									margin: "1em 0em 0em 1em",
								}}
								onClick={() => {
									toggleIsOpen();
									toggleIsCompleted();
									toggleIsDisabledRadio();
								}}
							>
								{/* {values[feeCategoryName].length === 0 ? `Add ${feeName} Fee` : `Add Another ${feeName} Fee`} */}
								{`Mark as complete`}
							</Button>
						</Grid>
					)}
				</AccordionActions>
			</Accordion>
		)
	);
}

export default React.memo(FeeAccordion);
