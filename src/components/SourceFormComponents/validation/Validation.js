import * as Yup from "yup";

const fixedFeeSchema = Yup.lazy((object) => {
	// if (object === "") {
	if (!object) {
		return Yup.mixed().notRequired();
	}
	return Yup.object().shape({
		feePrice: Yup.number()
			.required("Fee Net Price is required")
			// .nullable()
			.positive("Fee can only be a positive number")
			.min(0.01, "Fee Price must be above 0"),
		feeVat: Yup.number().notRequired(),
		feeTotalPrice: Yup.number().notRequired("Fee Total Price is required"),
	});
});
const referalfixedFeeSchema = Yup.lazy((object) => {
	// if (object === "") {
	if (!object) {
		return Yup.mixed().notRequired();
	}
	return Yup.object().shape({
		feePrice: Yup.number().notRequired(),
		// .required("Fee Net Price is required")
		// .nullable()
		// .positive("Fee can only be a positive number")
		// .min(0.01, "Fee Price must be above 0"),
		feeVat: Yup.number().notRequired(),
		feeTotalPrice: Yup.number().notRequired(),
	});
});

const variableFeeSchema = Yup.lazy((object) => {
	if (object === []) {
		return Yup.mixed().notRequired();
	}
	return Yup.array().of(
		Yup.object().shape({
			scaleMin: Yup.number()
				.required("Scale Min Value is required")
				.nullable()
				.positive("Scale Min Value can only be a positive number")
				.min(0, "Scale Min Value can not be a negative number"),
			scaleMax: Yup.number()
				.required("Scale Max Value is required")
				.nullable()
				.positive("Scale Max Value can only be a positive number")
				.moreThan(
					Yup.ref("scaleMin"),
					"Scale Max Value must be higher than the Scale Min Value"
				),
			feePrice: Yup.number("Fee Net Price has to be a number")
				.required("Fee Net Price is required")
				.positive("Fee can only be a positive number")
				.min(0.01, "Fee Net Price must be above 0"),
			feeVat: Yup.number().notRequired(),
			feeTotalPrice: Yup.number().notRequired("Fee Total Price is required"),
		})
	);
});

const mixedSchema = Yup.array().of(
	Yup.object().shape({
		fee_id: Yup.string().required("No Fee id was generated"),
		feeName: Yup.string().required("Fee name is required"),
		feeDescription: Yup.string().notRequired(),
		vatable: Yup.boolean().notRequired().default(true),
		feeOriginType: Yup.string().required().default("custom"),
		feeType: Yup.string().required("Fee Type is required"),
		fixedFee: fixedFeeSchema,
		variableFee: variableFeeSchema,
	})
);
const referalSchema = Yup.array().of(
	Yup.object().shape({
		fee_id: Yup.string().required("No Fee id was generated"),
		feeName: Yup.string().required("Fee name is required"),
		feeDescription: Yup.string().notRequired(),
		vatable: Yup.boolean().notRequired().default(true),
		feeOriginType: Yup.string().required().default("custom"),
		feeType: Yup.string().required("Fee Type is required"),
		fixedFee: referalfixedFeeSchema,
		variableFee: variableFeeSchema,
	})
);

export const validationSchema = Yup.object().shape({
	id: Yup.string().required("No source id was generated"),
	sourceName: Yup.string().required("Source Name is Required!"),
	introducers: Yup.string().notRequired(),
	BDM: Yup.string().notRequired().nullable(),
	expeditedDOEFeeChargeable: Yup.object().shape({
		sixToEightWeeks: Yup.boolean().notRequired().default(false),
		fourToSixWeeks: Yup.boolean().notRequired().default(false),
		lessThanFourWeeks: Yup.boolean().notRequired().default(false),
	}),
	comments: Yup.string().notRequired().nullable(),
	purchaseFees: mixedSchema.min(1, "Please add at least one Purchase Fee"),
	saleFees: mixedSchema.min(1, "Please add at least one Sale Fee"),
	remortgageFees: mixedSchema.min(1, "Please add at least one Remortgage  Fee"),
	generalFees: mixedSchema.min(1, "Please add at least one General Fee"),
	supplementSaleFees: mixedSchema.min(1, "Please add at least one Supplement Sale Fee"),
	supplementPurchaseFees: mixedSchema.min(1, "Please add at least one Supplement Purchase Fee"),
	supplementRemortgageFees: mixedSchema.min(
		1,
		"Please add at least one Supplement Remortgage Fee"
	),
	standardOutlaysFees: mixedSchema.min(1, "Please add at least one Outlay Fee"),
	referalFees: referalSchema,
});
