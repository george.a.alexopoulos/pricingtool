import React from "react";
//  Form, Formik, Field, ErrorMessage,
import { FieldArray } from "formik";
import { Grid, Button } from "@material-ui/core";
import Fee from "./Fee";
// import useStyles from "../hooks/useStyles";
// import { v4 as uuidv4 } from "uuid";

function FeeFieldArray(props) {
	// , handleChange
	// values,
	const { feeName, feeCategoryName, showAddFeeButtom, emptyFee, feeOrigin } = props;
	// console.log(values);
	// ``;
	// console.log("FeeFieldArray category name", feeCategoryName);
	// console.log("FeeFieldArray feeOrigin", feeOrigin);
	// console.log(feeName);
	// console.log(values[name]);
	// console.log(values[name].length);
	// console.log(values[name]);
	// console.log(fieldArrayValues);
	// console.log(values);

	return (
		<Grid item xs={11} md={12}>
			<FieldArray name={feeCategoryName}>
				{(props) => {
					// console.log("fieldArray props: ", props);
					const { push, remove, form } = props;
					const { setFieldValue } = form;
					// const { touched } = form;
					// console.log("Form from FieldArray", form);
					// console.log("Form from FieldArray", form.values[feeCategoryName].length);
					// console.log("Form from FieldArray", form.values[feeCategoryName]);
					// console.log(form.errors);
					// console.log(form);

					return (
						<>
							{form.values[feeCategoryName].map((_, index) => (
								<Grid item key={index}>
									<Fee
										// feeId={_.fee_id}
										feeName={feeName}
										_={_}
										index={index}
										remove={remove}
										push={push}
										// values={values}
										setFieldValue={setFieldValue}
										// form={form}
										// key={index}
										// touchedFeePrice={touched[name]}
										feeCategoryName={feeCategoryName}
										// handleChange={handleChange}
										// feeOrigin={feeOrigin}
									/>
								</Grid>
							))}
							{((feeOrigin === "custom" && form.values[feeCategoryName].length === 0) ||
								(!form.errors[feeCategoryName] && showAddFeeButtom)) && (
								<Grid item>
									<Button
										variant="contained"
										color="primary"
										style={{
											minWidth: "11em",
											height: "40px",
											margin: "1em 0em 0em 1em",
										}}
										onClick={() => {
											push(emptyFee);
										}}
									>
										{form.values[feeCategoryName].length === 0
											? `Add Custom ${feeName} Fee`
											: `Add Another Custom ${feeName} Fee`}
									</Button>
								</Grid>
							)}
						</>
					);
				}}
			</FieldArray>
		</Grid>
	);
}

export default React.memo(FeeFieldArray);
