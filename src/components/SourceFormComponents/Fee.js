import React, { useEffect } from "react";
import { Field, ErrorMessage, FastField } from "formik";
import {
	Grid,
	Typography,
	TextField,
	Divider,
	Tooltip,
	IconButton,
	Radio,
	RadioGroup,
	FormControlLabel,
	FormControl,
	FormLabel,
} from "@material-ui/core";
import { CheckboxWithLabel } from "formik-mui";

import { withStyles } from "@material-ui/core/styles";
import { MdDeleteForever } from "react-icons/md";

import useStyles from "../hooks/useStyles";
import FixedFee from "./FixedFee";
import VariableFee from "./VariableFee";
// import { v4 as uuidv4 } from "uuid";

const fixedEmptyFee = {
	feePrice: "",
	feeVat: 0.0,
	feeTotalPrice: 0.0,
};

const variableEmptyFee = [
	{
		scaleMin: "",
		scaleMax: "",
		feePrice: "",
		feeVat: 0.0,
		feeTotalPrice: 0.0,
	},
];

const DeleteTooltip = withStyles((theme) => ({
	arrow: {
		color: "red",
	},
	tooltip: {
		// backgroundColor: theme.palette.common.white,
		backgroundColor: "red",
		color: "theme.palette.common.white",
		boxShadow: theme.shadows[1],
		fontSize: 11,
	},
}))(Tooltip);

function Fee(props) {
	//  form,
	// feeId,
	// , touchedFeePrice
	// , handleChange
	// values,
	const { feeName, _, index, remove, setFieldValue, feeCategoryName } = props;
	const { feeType, feeOriginType } = _;
	// console.log("_", _);
	// console.log(_.feeOriginType);
	// console.log(feeOriginType);
	// console.log(feeOrigin);

	const disabled = feeOriginType === "standard" ? true : false;
	// console.log(disabled);
	// const { setErrors, setFieldError } = form;
	// console.log(_);
	// console.log(feeId);
	// console.log(values[feeCategoryName][index].fixedFee.feePrice);
	// console.log(values[feeCategoryName].feeId.fixedFee.feePrice);
	// console.log(values[feeCategoryName][feeId].fixedFee.feePrice);
	// console.log(feeType);
	// console.log("Values from SaleFee", values);
	// console.log(form);
	// console.log(touched);
	// console.log(touchedFeePrice);
	// console.log(touched.feeCategory[index].fixedFee.feePrice);
	// console.log(form.errors);
	// `${feeCategoryName}[${index}].feeName`
	// console.log(handleChange);

	useEffect(() => {
		if (feeOriginType === "standard") {
			return;
		} else if (feeOriginType === "custom") {
			if (feeType === "fixed") {
				setFieldValue(`${feeCategoryName}[${index}].fixedFee`, fixedEmptyFee, false);
				setFieldValue(`${feeCategoryName}[${index}].variableFee`, [], false);
				// setErrors({errors.feeCategoryName[index].variableFee: {}});
				// console.log(index);
				// console.log(variableEmptyFee);
				// setFieldValue(`${feeCategoryName}[${index}].variableFee`, variableEmptyFee);
				// setFieldError(`saleFees[${index}].variableFee`, {});
			} else if (feeType === "variable") {
				setFieldValue(`${feeCategoryName}[${index}].variableFee`, variableEmptyFee, false);
				setFieldValue(`${feeCategoryName}[${index}].fixedFee`, "", false);
				// setFieldValue(`${feeCategoryName}[${index}].fixedFee`, fixedEmptyFee);
				// resetForm({values:{...values, `saleFees[${index}].fixedFee`: fixedEmptyFee}})
				// setFieldError(`saleFees[${index}].fixedFee.feePrice`, "");
			}
		}
	}, [feeOriginType, feeType, feeCategoryName, setFieldValue, index]);

	const handleFeeTypeChange = (event) => {
		setFieldValue(`${feeCategoryName}[${index}].feeType`, event.currentTarget.value, false);
	};

	const classes = useStyles();

	return (
		<Grid
			container
			direction="column"
			spacing={2}
			style={{ position: "relative", width: "100%" }}
		>
			{!disabled && (
				<DeleteTooltip
					style={{
						position: "absolute",
						top: "0em",
						right: "0em",
						cursor: "pointer",
					}}
					arrow
					title="Delete Fee"
					placement="bottom"
					enterDelay={100}
					leaveDelay={150}
				>
					<IconButton aria-label="delete" onClick={() => remove(index)}>
						<MdDeleteForever size={30} color="red" />
					</IconButton>
				</DeleteTooltip>
			)}
			<Grid item xs={11} md={12}>
				<Typography variant="h6">{`${feeName} Fee ${index + 1}`}</Typography>
			</Grid>
			<Grid container item style={{ margin: "0em 0em 0em" }} spacing={2}>
				<Grid container item>
					<Grid item xs={11} sm="auto">
						<FastField
							className={classes.MuiFormControl}
							variant="outlined"
							margin="dense"
							name={`${feeCategoryName}[${index}].feeName`}
							as={TextField}
							label="Fee Name"
							color="primary"
							disabled={disabled}
						/>
						<ErrorMessage
							name={`${feeCategoryName}[${index}].feeName`}
							render={(msg) => <div className={classes.error}>{msg}</div>}
						/>
					</Grid>
					<Grid item xs={11} sm="auto">
						<FastField
							className={classes.MuiFormControl}
							variant="outlined"
							margin="dense"
							name={`${feeCategoryName}[${index}].feeDescription`}
							as={TextField}
							label="Fee Description"
							color="primary"
							fullWidth
							// style={{ width: "900px" }}
							style={{ width: "35rem" }}
							disabled={disabled}
						/>
					</Grid>
				</Grid>
				{_.feeName && (
					<Grid
						container
						item
						direction="column"
						spacing={2}
						justifyContent="flex-start"
						alignItems="flex-start"
						className={classes.checkboxContainer}
					>
						<Grid item xs={11} sm="auto">
							<FormControl component="fieldset">
								<FormLabel component="legend">Select Fee Type:</FormLabel>
								<RadioGroup
									name={`${feeCategoryName}[${index}].feeType`}
									value={_.feeType}
									onChange={handleFeeTypeChange}
								>
									<FormControlLabel
										value={"fixed"}
										control={<Radio color="primary" />}
										label="Fixed"
										className={classes.smallRadioButton}
										disabled={disabled}
									/>
									<FormControlLabel
										value={"variable"}
										control={<Radio />}
										label="Variable"
										className={classes.smallRadioButton}
										disabled={disabled}
									/>
								</RadioGroup>
							</FormControl>
						</Grid>
						<Grid item xs={11} sm="auto">
							<Field
								name={`${feeCategoryName}[${index}].vatable`}
								// value={_.vatable}
								type="checkbox"
								component={CheckboxWithLabel}
								Label={{ label: "Apply VAT" }}
								color="primary"
								disabled={disabled}
								// onChange={(event) => {
								// 	setFieldValue(`saleFees[${index}].vatable`, event.currentTarget.value);
								// }}
							/>
						</Grid>
					</Grid>
				)}
				{feeType === "fixed" && (
					<FixedFee
						setFieldValue={setFieldValue}
						vatable={_.vatable}
						feeType={feeType}
						// values={values}
						index={index}
						fee={_}
						feeCategoryName={feeCategoryName}
						// touchedFixedFee={touchedFeePrice[index].fixedFee || false}
						disabled={disabled}
						// handleChange={handleChange}
					/>
				)}
				{feeType === "variable" && (
					<VariableFee
						setFieldValue={setFieldValue}
						vatable={_.vatable}
						feeType={feeType}
						// values={values}
						index={index}
						fee={_}
						feeCategoryName={feeCategoryName}
						// touchedVariableFee={touchedFeePrice[index].variableFee || false}
						disabled={disabled}
						// handleChange={handleChange}
					/>
				)}
			</Grid>

			<Divider style={{ width: "100%", margin: "0rem 0rem 1rem 0rem" }} />
		</Grid>
	);
}

export default React.memo(Fee);
