import React from "react";
// import { useHistory } from "react-router-dom";
import { Form, Formik, ErrorMessage, FastField } from "formik";
import {
	Card,
	CardContent,
	Grid,
	Typography,
	TextField,
	MenuItem,
	Button,
	CircularProgress,
	Divider,
	FormLabel,
	FormControl,
	FormGroup,
	FormHelperText,
} from "@material-ui/core";

import { CheckboxWithLabel } from "formik-mui";
import useStyles from "../hooks/useStyles";
// import useToggle from "../hooks/useToggle";
import { validationSchema } from "./validation/Validation";
import FeeAccordion from "./FeeAccordion";
import { v4 as uuidv4 } from "uuid";
import StandardSaleFees from "./standardFees/StandardSaleFees";
import StandardPurchaseFees from "./standardFees/StandardPurchaseFees";
import StandardRemortgageTOEFees from "./standardFees/StandardRemortgageTOEFees";
import StandardGeneralFees from "./standardFees/StandardGeneralFees";
import SupplementSaleAndPurchaseFees from "./standardFees/SupplementSaleAndPurchaseFees";
import SupplementRemortgageFees from "./standardFees/SupplementRemortgageFees";
import StandardOutlays from "./standardFees/StandardOutlays";
import ReferalFees from "./standardFees/ReferalFees";

// import Autocomplete from "@material-ui/lab/Autocomplete";
// import Autocomplete from "@mui/material/Autocomplete";

function SourceForm() {
	const initialValues = {
		id: uuidv4(),
		sourceName: "",
		introducers: "",
		BDM: "",
		expeditedDOEFeeChargeable: {
			sixToEightWeeks: false,
			fourToSixWeeks: false,
			lessThanFourWeeks: false,
		},
		comments: "",
		purchaseFees: [],
		saleFees: [],
		remortgageFees: [],
		generalFees: [],
		supplementSaleFees: [],
		supplementPurchaseFees: [],
		supplementRemortgageFees: [],
		standardOutlaysFees: [],
		referalFees: [],
	};

	// console.log(StandardSaleFees);
	// const [openPurchases, toggleOpenPurchases] = useToggle(true);
	// const [isCompletePurchases, toggleIsCompletePurchases] = useToggle(false);
	// const [openRemortgages, toggleOpenRemortgages] = useToggle(true);
	// const [isCompleteRemortgages, toggleIsCompleteRemortgages] = useToggle(false);
	// useEffect(() => {
	// 	console.log(initialFormValues);
	// 	// console.log(initialsaleFees);
	// 	// setInitialFormValues((prevState) => ({
	// 	// 	...prevState,
	// 	// 	saleFees: initialsaleFees,
	// 	// }));
	// }, [initialFormValues]);

	// const history = useHistory();

	const onSubmit = (values, onSubmitProps) => {
		setTimeout(() => {
			// console.log("Form Data", values);
			onSubmitProps.setSubmitting(false);
			onSubmitProps.resetForm();
			// history.push("/");
			// localStorage.setItem("source", JSON.stringify(values));
		}, 500);
	};

	const classes = useStyles();

	return (
		<Card className={classes.root}>
			<CardContent>
				<Formik
					initialValues={initialValues}
					validationSchema={validationSchema}
					onSubmit={onSubmit}
					validateOnMount={true}
					validateOnChange={true}
					// validateOnBlur={true}
					// initialValues={formValues || initialValues}
					// the above is used if we want to initialize the form with already saved values
					// enableReinitialize
				>
					{(formik) => {
						// console.log("Formik values", formik.values);
						// , handleChange
						// console.log("Formik props", formik);
						const { values, setFieldValue, isValid, errors } = formik;
						// console.log(values.saleFees);
						// console.log(values.sourceName);
						// console.log(isValid);
						// console.log(errors);
						// console.log(errors["saleFees"]);
						return (
							<Form>
								<Grid container direction="column" spacing={2}>
									<Grid
										container
										style={{ margin: "0em 1em 0em 1em", paddingRight: "2em" }}
									>
										<Grid item xs={11} md={12}>
											<Typography variant="h3">New source</Typography>
											<Divider
												style={{ width: "100%", margin: "1em 0em 1em 0em" }}
											/>
										</Grid>
										<Grid item xs={11} md={12}>
											<Typography variant="h4">Source Details</Typography>
										</Grid>
									</Grid>
									<Grid
										container
										direction="column"
										style={{ margin: "0em 1em 0em 1em", paddingRight: "2em" }}
									>
										<Grid
											container
											direction="column"
											spacing={4}
											style={{ marginTop: "0.5em" }}
										>
											<Grid item xs={11} md={6}>
												<FastField
													variant="outlined"
													className={classes.MuiFormControl}
													margin="dense"
													required
													name="sourceName"
													id="sourceName"
													as={TextField}
													label="Source Name"
													color="primary"
													fullWidth
												/>
												<ErrorMessage
													name="sourceName"
													render={(msg) => (
														<div className={classes.error}>{msg}</div>
													)}
												/>
											</Grid>
											<Grid item xs={11} md={6}>
												<FastField
													variant="outlined"
													className={classes.MuiFormControl}
													margin="dense"
													// required
													name="introducers"
													id="introducers"
													as={TextField}
													label="Introducers"
													color="primary"
													fullWidth
												/>
												<ErrorMessage
													name="introducers"
													render={(msg) => (
														<div className={classes.error}>{msg}</div>
													)}
												/>
											</Grid>
											<Grid item xs={11} s={11} md={6}>
												<FastField
													variant="outlined"
													className={classes.MuiFormControl}
													margin="dense"
													// required
													name="BDM"
													id="BDM"
													as={TextField}
													label="BDM"
													color="primary"
													fullWidth
													select
												>
													<MenuItem value={"OTHER"}>OTHER</MenuItem>
													<MenuItem value={"AA"}>AA</MenuItem>
													<MenuItem value={"HMcT"}>HMcT</MenuItem>
													<MenuItem value={"NC"}>NC</MenuItem>
													<MenuItem value={"OM"}>OM</MenuItem>
													<MenuItem value={"TMcA"}>TMcA</MenuItem>
												</FastField>
												<ErrorMessage name="BDM" />
											</Grid>
										</Grid>
										<Grid
											container
											spacing={2}
											className={classes.checkboxContainer}
										>
											<Grid item xs={11} md={8}>
												<FormControl
													component="fieldset"
													// className={classes.checkboxControl}
												>
													<FormLabel
														component="legend"
														style={{ margin: "0.5em 0em 0.5em 0em" }}
													>
														Expedited DOE Fee Chargeable
													</FormLabel>
													<FormGroup>
														<FastField
															type="checkbox"
															component={CheckboxWithLabel}
															name="expeditedDOEFeeChargeable.sixToEightWeeks"
															Label={{ label: "6 to 8 Weeks" }}
														/>
														<FastField
															type="checkbox"
															component={CheckboxWithLabel}
															name="expeditedDOEFeeChargeable.fourToSixWeeks"
															Label={{ label: "4 to 6 Weeks" }}
														/>
														<FastField
															type="checkbox"
															component={CheckboxWithLabel}
															name="expeditedDOEFeeChargeable.lessThanFourWeeks"
															Label={{ label: "Less than 4 Weeks" }}
														/>
													</FormGroup>
													<FormHelperText>
														Select one or more
													</FormHelperText>
												</FormControl>
											</Grid>
										</Grid>
										<Grid
											item
											// spacing={2}
											className={classes.checkboxContainer}
										>
											<Grid item xs={11} md={8}>
												<FastField
													variant="outlined"
													margin="dense"
													// required
													name="comments"
													id="comments"
													as={TextField}
													// as="textarea"
													label="Additional Comments"
													color="primary"
													fullWidth
													multiline
													rows={4}
												/>
												<ErrorMessage name="comments" />
											</Grid>
										</Grid>
									</Grid>
									{/* <Divider style={{ width: "100%", margin: "2em 0em 1em 0em" }} /> */}
									<Divider style={{ width: "100%", margin: "2em 0em 1em 0em" }} />
									<Grid
										container
										spacing={2}
										style={{ margin: "1em 1em 0em 1em", paddingRight: "2em" }}
									>
										<Grid item xs={11} md={12}>
											<Typography
												variant="h4"
												style={{ margin: "0em 0em 1em 0em" }}
											>
												Source Fees
											</Typography>
											{/* <Divider style={{ width: "100%", margin: "2em 0em 1em 0em" }} /> */}
										</Grid>
										<Grid item xs={12} style={{ margin: "1em 0em 1em 0em" }}>
											<FeeAccordion
												feeName="Purchase Conveyancing"
												// formik={formik}
												feeCategoryName="purchaseFees"
												values={values}
												setFieldValue={setFieldValue}
												// isValid={isValid}
												StandardFees={StandardPurchaseFees}
												errors={errors}
											/>
										</Grid>
										<Grid item xs={12} style={{ margin: "1em 0em 1em 0em" }}>
											<FeeAccordion
												feeName="Sale Conveyancing"
												feeCategoryName="saleFees"
												values={values}
												setFieldValue={setFieldValue}
												// isValid={isValid}
												StandardFees={StandardSaleFees}
												errors={errors}
											/>
										</Grid>
										<Grid item xs={12} style={{ margin: "1em 0em 1em 0em" }}>
											<FeeAccordion
												feeName="Remortgage Conveyancing"
												// formik={formik}
												feeCategoryName="remortgageFees"
												values={values}
												setFieldValue={setFieldValue}
												// isValid={isValid}
												StandardFees={StandardRemortgageTOEFees}
												errors={errors}
											/>
										</Grid>
										<Grid item xs={12} style={{ margin: "1em 0em 1em 0em" }}>
											<FeeAccordion
												feeName="General"
												// formik={formik}
												feeCategoryName="generalFees"
												values={values}
												setFieldValue={setFieldValue}
												// isValid={isValid}
												StandardFees={StandardGeneralFees}
												errors={errors}
											/>
										</Grid>
										<Grid item xs={12} style={{ margin: "1em 0em 1em 0em" }}>
											<FeeAccordion
												feeName="Supplement Sale"
												// formik={formik}
												feeCategoryName="supplementSaleFees"
												values={values}
												setFieldValue={setFieldValue}
												// isValid={isValid}
												StandardFees={SupplementSaleAndPurchaseFees}
												errors={errors}
											/>
										</Grid>
										<Grid item xs={12} style={{ margin: "1em 0em 1em 0em" }}>
											<FeeAccordion
												feeName="Supplement Purchase"
												// formik={formik}
												feeCategoryName="supplementPurchaseFees"
												values={values}
												setFieldValue={setFieldValue}
												// isValid={isValid}
												StandardFees={SupplementSaleAndPurchaseFees}
												errors={errors}
											/>
										</Grid>
										<Grid item xs={12} style={{ margin: "1em 0em 1em 0em" }}>
											<FeeAccordion
												feeName="Supplement Remortgage and TOE"
												// formik={formik}
												feeCategoryName="supplementRemortgageFees"
												values={values}
												setFieldValue={setFieldValue}
												// isValid={isValid}
												StandardFees={SupplementRemortgageFees}
												errors={errors}
											/>
										</Grid>
										<Grid item xs={12} style={{ margin: "1em 0em 1em 0em" }}>
											<FeeAccordion
												feeName="Outlays"
												// formik={formik}
												feeCategoryName="standardOutlaysFees"
												values={values}
												setFieldValue={setFieldValue}
												// isValid={isValid}
												StandardFees={StandardOutlays}
												errors={errors}
											/>
										</Grid>
										<Grid item xs={12} style={{ margin: "1em 0em 1em 0em" }}>
											<FeeAccordion
												feeName="Referal"
												// formik={formik}
												feeCategoryName="referalFees"
												values={values}
												setFieldValue={setFieldValue}
												// isValid={isValid}
												StandardFees={ReferalFees}
												errors={errors}
											/>
										</Grid>
									</Grid>
									<Grid
										container
										spacing={6}
										justifyContent={"center"}
										style={{ marginTop: "1em" }}
									>
										<Grid item>
											<Button
												// disabled={isValid || formik.isSubmitting}
												disabled={!isValid}
												variant="contained"
												color="primary"
												className={classes.saveButton}
												type="submit"
												startIcon={
													formik.isSubmitting ? (
														<CircularProgress size="1rem" />
													) : undefined
												}
											>
												{formik.isSubmitting ? "SAVING" : "SAVE SOURCE"}
											</Button>
										</Grid>
										<Grid item>
											<Button
												variant="contained"
												color="secondary"
												href="/"
												className={classes.cancelButton}
											>
												CANCEL
											</Button>
										</Grid>
									</Grid>
								</Grid>

								{/* <pre>{JSON.stringify(formik.isValid, null, 4)}</pre> */}
								{/* <pre>{JSON.stringify(formik.errors, null, 4)}</pre> */}

								{/* <pre>{JSON.stringify(formik.values.saleFees, null, 4)}</pre> */}
								<pre>{JSON.stringify(formik.values.PurchaseFees, null, 4)}</pre>
								<pre>{JSON.stringify(formik.values, null, 4)}</pre>
							</Form>
						);
					}}
				</Formik>
			</CardContent>
		</Card>
	);
}

export default React.memo(SourceForm);
