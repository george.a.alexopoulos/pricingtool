import React, { createContext, useState, useContext } from "react";

const SelectedOutlaysFeesContext = createContext();

// const initialValues = {
// 	source: "",
// 	service: "",
// 	conveyancingPrice: "",
// };

export const SelectedOutlaysFeesProvider = ({ children }) => {
	const [selectedOutlaysFees, setSelectedOutlaysFees] = useState([]);

	// const setValues = (values) => {
	// 	setData((prevData) => ({
	// 		...prevData,
	// 		...values,
	// 	}));
	// };
	return (
		<SelectedOutlaysFeesContext.Provider
			value={{ selectedOutlaysFees, setSelectedOutlaysFees }}
		>
			{children}
		</SelectedOutlaysFeesContext.Provider>
	);
};

export const useSelectedOutlaysFees = () => useContext(SelectedOutlaysFeesContext);
