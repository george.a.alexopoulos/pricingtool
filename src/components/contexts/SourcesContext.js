import React, { createContext } from "react";
import sources from "../../assets/sources";

export const SourcesContext = createContext();

function SourcesContextProvider({ children }) {
	return <SourcesContext.Provider value={{ sources }}>{children}</SourcesContext.Provider>;
}

export default SourcesContextProvider;

// const SourcesContextProvider = ({ children }) => {
// 	// const [sources, setSources] = useState(sources);

// 	// const setValues = (values) => {
// 	// 	setData((prevData) => ({
// 	// 		...prevData,
// 	// 		...values,
// 	// 	}));
// 	// };
// 	return (
// 		<SourcesContext.Provider value={{ sources }}>
// 			{children}
// 		</SourcesContext.Provider>
// 	);
// };

// export default SourcesContextProvider;
