import React, { createContext, useState, useContext } from "react";

const SelectedSupplementFeesContext = createContext();

// const initialValues = {
// 	source: "",
// 	service: "",
// 	conveyancingPrice: "",
// };

export const SelectedSupplementFeesProvider = ({ children }) => {
	const [selectedSupplementFees, setSelectedSupplementFees] = useState([]);

	// const setValues = (values) => {
	// 	setData((prevData) => ({
	// 		...prevData,
	// 		...values,
	// 	}));
	// };
	return (
		<SelectedSupplementFeesContext.Provider
			value={{ selectedSupplementFees, setSelectedSupplementFees }}
		>
			{children}
		</SelectedSupplementFeesContext.Provider>
	);
};

export const useSelectedSupplementFees = () => useContext(SelectedSupplementFeesContext);
