import React, { createContext, useState, useContext } from "react";

const SelectedDataContext = createContext();

// const initialValues = {
// 	source: "",
// 	service: "",
// 	conveyancingPrice: "",
// };

export const SelectedDataProvider = ({ children }) => {
	const [data, setData] = useState();

	const setValues = (values) => {
		setData((prevData) => ({
			...prevData,
			...values,
		}));
	};
	return (
		<SelectedDataContext.Provider value={{ data, setData, setValues }}>
			{children}
		</SelectedDataContext.Provider>
	);
};

export const useData = () => useContext(SelectedDataContext);
