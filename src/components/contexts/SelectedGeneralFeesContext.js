import React, { createContext, useState, useContext } from "react";

const SelectedGeneralFeesContext = createContext();

// const initialValues = {
// 	source: "",
// 	service: "",
// 	conveyancingPrice: "",
// };

export const SelectedGeneralFeesProvider = ({ children }) => {
	const [selectedGeneralFees, setSelectedGeneralFees] = useState([]);

	// const setValues = (values) => {
	// 	setData((prevData) => ({
	// 		...prevData,
	// 		...values,
	// 	}));
	// };
	return (
		<SelectedGeneralFeesContext.Provider
			value={{ selectedGeneralFees, setSelectedGeneralFees }}
		>
			{children}
		</SelectedGeneralFeesContext.Provider>
	);
};

export const useSelectedGeneralFees = () => useContext(SelectedGeneralFeesContext);
