import React, { createContext, useState, useContext } from "react";

const SelectedServiceFeesContext = createContext();

// const initialValues = {
// 	source: "",
// 	service: "",
// 	conveyancingPrice: "",
// };

export const SelectedServiceFeesProvider = ({ children }) => {
	const [selectedServiceFees, setSelectedServiceFees] = useState();

	// const setValues = (values) => {
	// 	setData((prevData) => ({
	// 		...prevData,
	// 		...values,
	// 	}));
	// };
	return (
		<SelectedServiceFeesContext.Provider
			value={{ selectedServiceFees, setSelectedServiceFees }}
		>
			{children}
		</SelectedServiceFeesContext.Provider>
	);
};

export const useSelectedServiceFees = () => useContext(SelectedServiceFeesContext);
