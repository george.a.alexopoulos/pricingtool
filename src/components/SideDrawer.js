import React from "react";
import { Switch, Route, Link } from "react-router-dom";
import Home from "./Home";
import SourceForm from "./SourceFormComponents/SourceForm";
// import Calculator from "./Calculator";
import Calculator from "./CalculatorComponents/Calculator";
import clsx from "clsx";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import CssBaseline from "@material-ui/core/CssBaseline";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import List from "@material-ui/core/List";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import HomeIcon from "@material-ui/icons/Home";
// import { IoMdPersonAdd } from "react-icons/io";
import { MdCalculate } from "react-icons/md";
import CalculatorStep2 from "./CalculatorComponents/CalculatorStep2";
import CalculatorStep3 from "./CalculatorComponents/CalculatorStep3";
import CalculatorStep4 from "./CalculatorComponents/CalculatorStep4";
import CalculatorStep5 from "./CalculatorComponents/CalculatorStep5";
import CalculatorResults from "./CalculatorComponents/CalculatorResults";
import SourcesContextProvider from "./contexts/SourcesContext";
import { SelectedDataProvider } from "./contexts/SelectedDataContext";
import { SelectedServiceFeesProvider } from "./contexts/SelectedServiceFeesContext";
import { SelectedGeneralFeesProvider } from "./contexts/SelectedGeneralFeesContext";
import { SelectedSupplementFeesProvider } from "./contexts/SelectedSupplementFeesContext";
import { SelectedOutlaysFeesProvider } from "./contexts/SelectedOutlaysFeesContext";
// import Autocomplete from "@material-ui/lab/Autocomplete";

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
	root: {
		display: "flex",
		// marginRight: "2.2rem",
		marginRight: "36px",
		// marginLeft: "36px",
		// paddingRight: "24px",
	},
	appBar: {
		transition: theme.transitions.create(["margin", "width"], {
			easing: theme.transitions.easing.sharp,
			duration: theme.transitions.duration.leavingScreen,
		}),
	},
	appBarShift: {
		width: `calc(100% - ${drawerWidth}px)`,
		marginLeft: drawerWidth,
		transition: theme.transitions.create(["margin", "width"], {
			easing: theme.transitions.easing.easeOut,
			duration: theme.transitions.duration.enteringScreen,
		}),
	},
	menuButton: {
		marginRight: theme.spacing(2),
	},
	hide: {
		display: "none",
	},
	drawer: {
		width: drawerWidth,
		flexShrink: 0,
	},
	drawerPaper: {
		width: drawerWidth,
	},
	drawerHeader: {
		display: "flex",
		alignItems: "center",
		padding: theme.spacing(0, 1),
		// necessary for content to be below app bar
		...theme.mixins.toolbar,
		justifyContent: "flex-end",
	},
	content: {
		flexGrow: 1,
		padding: theme.spacing(3),
		// marginRight: theme.spacing(5),
		transition: theme.transitions.create("margin", {
			easing: theme.transitions.easing.sharp,
			duration: theme.transitions.duration.leavingScreen,
		}),
		marginLeft: -drawerWidth,
	},
	contentShift: {
		transition: theme.transitions.create("margin", {
			easing: theme.transitions.easing.easeOut,
			duration: theme.transitions.duration.enteringScreen,
		}),
		marginLeft: 0,
	},
	link: {
		textDecoration: "none",
		color: theme.palette.text.primary,
	},
}));

export default function PersistentDrawerLeft() {
	const classes = useStyles();
	const theme = useTheme();
	const [open, setOpen] = React.useState(false);

	const handleDrawerOpen = () => {
		setOpen(true);
	};

	const handleDrawerClose = () => {
		setOpen(false);
	};

	return (
		<div className={classes.root}>
			<CssBaseline />
			<AppBar
				position="fixed"
				className={clsx(classes.appBar, {
					[classes.appBarShift]: open,
				})}
			>
				<Toolbar>
					<IconButton
						color="inherit"
						aria-label="open drawer"
						onClick={handleDrawerOpen}
						edge="start"
						className={clsx(classes.menuButton, open && classes.hide)}
					>
						<MenuIcon />
					</IconButton>
					<Typography variant="h6" noWrap>
						Pricing Tool
					</Typography>
				</Toolbar>
			</AppBar>
			<Drawer
				className={classes.drawer}
				variant="persistent"
				anchor="left"
				open={open}
				classes={{
					paper: classes.drawerPaper,
				}}
			>
				<div className={classes.drawerHeader}>
					<IconButton onClick={handleDrawerClose}>
						{theme.direction === "ltr" ? <ChevronLeftIcon /> : <ChevronRightIcon />}
					</IconButton>
				</div>
				<Divider />
				<List>
					<Link to="/" className={classes.link}>
						<ListItem button>
							<ListItemIcon>
								<HomeIcon />
							</ListItemIcon>
							<ListItemText primary={"HOME"} />
						</ListItem>
					</Link>
				</List>
				{/* <Divider />
				<List>
					<Link to="/newsource" className={classes.link}>
						<ListItem button>
							<ListItemIcon>
								<IoMdPersonAdd size={20} />
							</ListItemIcon>
							<ListItemText primary={"ADD NEW SOURCE"} />
						</ListItem>
					</Link>
				</List> */}
				<Divider />
				<List>
					<Link to="/calculator" className={classes.link}>
						<ListItem button>
							<ListItemIcon>
								<MdCalculate size={20} />
							</ListItemIcon>
							<ListItemText primary={"INTRODUCER PRICING"} />
						</ListItem>
					</Link>
				</List>
				<Divider />
			</Drawer>
			<main
				className={clsx(classes.content, {
					[classes.contentShift]: open,
				})}
			>
				<SourcesContextProvider>
					<SelectedDataProvider>
						<SelectedServiceFeesProvider>
							<SelectedGeneralFeesProvider>
								<SelectedSupplementFeesProvider>
									<SelectedOutlaysFeesProvider>
										<Switch>
											<Route exact path="/" component={Home} />
											<Route exact path="/newsource" component={SourceForm} />
											<Route
												exact
												path="/calculator"
												component={Calculator}
											/>
											<Route
												exact
												path="/calculator/step2"
												render={(props) => (
													// <SelectedServiceFeesProvider>
													<CalculatorStep2 {...props} />
													// </SelectedServiceFeesProvider>
												)}
											/>
											<Route
												exact
												path="/calculator/step3"
												render={(props) => (
													// <SelectedGeneralFeesProvider>
													<CalculatorStep3 {...props} />
													// </SelectedGeneralFeesProvider>
												)}
											/>
											<Route
												exact
												path="/calculator/step4"
												render={(props) => (
													// <SelectedSupplementFeesProvider>
													<CalculatorStep4 {...props} />
													// </SelectedSupplementFeesProvider>
												)}
											/>
											<Route
												exact
												path="/calculator/step5"
												render={(props) => (
													// <SelectedOutlaysFeesProvider>
													<CalculatorStep5 {...props} />
													// </SelectedOutlaysFeesProvider>
												)}
											/>
											<Route
												exact
												path="/calculator/results"
												render={(props) => <CalculatorResults {...props} />}
											/>
										</Switch>
									</SelectedOutlaysFeesProvider>
								</SelectedSupplementFeesProvider>
							</SelectedGeneralFeesProvider>
						</SelectedServiceFeesProvider>
					</SelectedDataProvider>
				</SourcesContextProvider>
			</main>
		</div>
	);
}
