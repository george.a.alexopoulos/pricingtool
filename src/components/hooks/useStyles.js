// import React from "react";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
	root: {
		width: "100%",
		margin: "50px 20px 10px 20px",
		padding: "10px",
		position: "relative",
		"& .MuiAccordionSummary-root:hover, .MuiButtonBase-root:hover": {
			cursor: "default",
		},
	},
	MuiGrid: {
		width: "100%",
	},
	MuiPaper: {
		width: "100s%",
	},
	MuiFormControl: {
		margin: "1em 1em 1em",
		padding: "0px",
		// height: "40px",
		width: "350px",
		zIndex: "3",
		fontSize: "medium",
		"& input": {
			height: "40px",
			padding: "0px 10px 0px",
			margin: "0px 0px 0px",
			border: "none",

			fontSize: "medium",
			width: "100%",
		},
		"& label": {
			padding: "10px 0px 10px 0px",
			// height: "34px",
			top: "-16px",
			fontSize: "small",
			justifyContent: "center",
			alignItem: "center",
			zIndex: "3",
			border: "none",
		},
		"& .MuiInputLabel-shrink": {
			zIndex: "1",
			padding: "4px 0px 0px 0px",
			marginBottom: "5px",
			top: "-6px",
			height: "10px",
		},
	},
	MuiOutlinedInputCurrencyInput: {
		// "&:hover": {
		// 	border: "1px solid #3f51b5",
		// },
	},
	MuiFormControlPrice: {
		margin: "1em 2em 1em 1em",
		padding: "0px",
		// height: "40px",
		width: "13em",
		zIndex: "3",
		fontSize: "medium",
		"& input": {
			height: "40px",
			padding: "0px 10px 0px",
			margin: "0px 0px 0px",
			border: "none",
			// border: "0.5px solid #4A90E2",

			fontSize: "medium",
			width: "100%",
		},
		"& label": {
			padding: "10px 0px 10px 0px",
			// height: "34px",
			top: "-16px",
			fontSize: "medium",
			justifyContent: "center",
			alignItem: "center",
			zIndex: "3",
			border: "none",
		},
		"& .MuiInputLabel-shrink": {
			zIndex: "-1",
			padding: "4px 0px 0px 0px",
			marginBottom: "5px",
			top: "-12px",
			height: "10px",
		},
	},
	saveButton: {
		height: "40px",
		minWidth: "10rem",
	},
	cancelButton: {
		height: "40px",
		minWidth: "10rem",
	},
	error: {
		margin: "0em 0em 1em 1em",
		padding: "0em 0em 0em 1em",
		width: "13em",
		color: "red",
		fontSize: "xs",
	},
	checkboxContainer: {
		width: "97%",
		margin: "1em 1em 1em",
		padding: "0px",
		fontSize: "medium",
	},
	smallRadioButton: {
		cursor: "pointer",
		"& span": {
			fontSize: "100%",
		},
		"& svg": {
			width: "0.75em",
			height: "0.75em",
		},
	},
	xsmallRadioButton: {
		cursor: "pointer",
		"& span": {
			fontSize: "90%",
		},
		"& svg": {
			width: "0.75em",
			height: "0.75em",
		},
	},
	// accordionHeading: {
	// 	fontSize: theme.typography.pxToRem(15),
	// },
	// accordionSecondaryHeading: {
	// 	fontSize: theme.typography.pxToRem(15),
	// 	color: theme.palette.text.secondary,
	// },
	// accordionIcon: {
	// 	verticalAlign: "bottom",
	// 	height: 20,
	// 	width: 20,
	// },
	// accordionDetails: {
	// 	alignItems: "center",
	// },
	// accordionColumn: {
	// 	flexBasis: "33.33%",
	// },
	// accordionHelper: {
	// 	borderLeft: `2px solid ${theme.palette.divider}`,
	// 	padding: theme.spacing(1, 2),
	// },
}));

export default useStyles;
