import React, { useState, useContext } from "react";
import { SourcesContext } from "../contexts/SourcesContext";
import { useData } from "../contexts/SelectedDataContext";
import { useSelectedServiceFees } from "../contexts/SelectedServiceFeesContext";
import { useHistory } from "react-router";
import { Card, CardContent, Typography } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core/styles";
import { alpha } from "@material-ui/core/styles/colorManipulator";

import { DataGrid } from "@material-ui/data-grid";
// import { array } from "yup/lib/locale";

const useStyles = makeStyles((theme) => ({
	// root: {
	// 	flexGrow: 1,
	// },
	root: {
		margin: theme.spacing(3, 0, 2),
		// margin: "50px 20px 10px 20px",
		width: "100%",
		textAlign: "center",
		padding: "10px",
		// fontFamily: "Lato",
		fontSize: 12,
		// position: "relative",
	},
	MuiGrid: {
		width: "100%",
	},
	saveButton: {
		margin: "2em 2em 0em 2em",
		height: "40px",
		minWidth: "10rem",
		"&:hover": {
			backgroundColor: alpha(theme.palette.primary.main, 0.5),
		},
	},
}));

function CalculatorStep2() {
	const [selectedRows, setSelectedRows] = useState([]);
	const { sources } = useContext(SourcesContext);
	const { setSelectedServiceFees } = useSelectedServiceFees();
	const { data: selectedData } = useData();
	const selectedSource = selectedData.source;
	const selectedService = selectedData.service;
	const conveyancingPrice = selectedData.conveyancingPrice;
	const filteredSource = sources.filter((source) => source.sourceName === selectedSource);

	// console.log(filteredSource);
	// console.log(filteredSource[0].saleFees[0].variableFee);

	let filteredFees = [];
	if (selectedService === "Sale Conveyancing") {
		filteredFees = filteredSource[0].saleFees;
	} else if (selectedService === "Purchase Conveyancing") {
		filteredFees = filteredSource[0].purchaseFees;
	} else {
		filteredFees = filteredSource[0].remortgageFees;
	}
	// console.log(filteredFees);

	// console.log(conveyancingPrice);

	const selectedFeePrice = (fee) => {
		if (fee.feeType === "fixed") {
			return new Intl.NumberFormat("en-GB", {
				style: "currency",
				currency: "GBP",
			}).format(fee.fixedFee.feePrice);
		} else {
			const saleVarFee = filteredSource[0].saleFees[0].variableFee.filter(
				(varFee) =>
					varFee.scaleMin < conveyancingPrice && varFee.scaleMax > conveyancingPrice
			);
			// console.log(saleVarFee);
			return new Intl.NumberFormat("en-GB", {
				style: "currency",
				currency: "GBP",
			}).format(saleVarFee[0].feePrice);
		}
	};

	const rows2 = filteredFees.map((fee) => {
		let newFee = {};
		newFee.id = fee.fee_id;
		newFee.feeName = fee.feeName;
		newFee.feePrice = selectedFeePrice(fee);
		newFee.feeCategory = selectedService;
		newFee.feeVatable = fee.vatable;
		newFee.feeDescription = fee.feeDescription;

		return newFee;
	});

	// console.log(rows2);

	const columns2 = [
		{ field: "id", headerName: "FEE ID", width: 90, hide: true },
		{
			field: "feeName",
			headerName: "Fee Name",
			width: 275,
			editable: false,
			sortable: false,
		},
		{
			field: "feePrice",
			headerName: "Fee Price",
			width: 100,
			editable: false,
			sortable: false,
		},
		{
			field: "feeCategory",
			headerName: "Fee Category",
			width: 250,
			editable: false,
			sortable: false,
		},
		{ field: "feeVatable", headerName: "FEE VATABLE", width: 90, hide: true },
		{
			field: "feeDescription",
			headerName: "Fee Description",
			width: 490,
			editable: false,
			sortable: false,
		},
	];

	// console.log(columns2);

	const history = useHistory();

	const classes = useStyles();
	return (
		<Card className={classes.root}>
			<CardContent>
				<Typography variant="h2" style={{ margin: "1em 0em 0.5em 0em" }}>
					{`${selectedService} Fees`}
				</Typography>
				<Typography variant="h6" style={{ margin: "1em 0em 0.5em 0em" }}>
					Select fees that apply
				</Typography>
				<div style={{ height: 425, width: "100%" }}>
					<DataGrid
						rows={rows2}
						columns={columns2}
						disableColumnMenu
						// pagination={false}
						pageSize={10}
						rowsPerPageOptions={[10]}
						checkboxSelection
						disableSelectionOnClick
						onSelectionModelChange={(ids) => {
							const selectedIDs = new Set(ids);
							const selectedRows = rows2.filter((row) => selectedIDs.has(row.id));
							setSelectedRows(selectedRows);
						}}
					/>
				</div>
				<Button
					variant="contained"
					color="primary"
					className={classes.saveButton}
					onClick={() => {
						history.push("/calculator");
					}}
				>
					BACK
				</Button>
				<Button
					variant="contained"
					color="primary"
					className={classes.saveButton}
					onClick={() => {
						// console.log(selectedRows)
						setSelectedServiceFees(selectedRows);
						history.push("/calculator/step3");
					}}
				>
					NEXT
				</Button>

				{/* <pre style={{ fontSize: 10 }}>{JSON.stringify(selectedRows, null, 4)}</pre> */}
			</CardContent>
		</Card>
	);
}

export default CalculatorStep2;

// const columns = [
// 	{ field: "fee_id", headerName: "FEE ID", width: 90, hide: true },
// 	{
// 		field: "firstName",
// 		headerName: "First name",
// 		width: 150,
// 		editable: false,
// 		sortable: false,
// 	},
// 	{
// 		field: "lastName",
// 		headerName: "Last name",
// 		width: 150,
// 		editable: false,
// 		sortable: false,
// 	},
// 	{
// 		field: "age",
// 		headerName: "Age",
// 		type: "number",
// 		width: 110,
// 		editable: true,
// 		sortable: false,
// 	},
// 	{
// 		field: "fullName",
// 		headerName: "Full name",
// 		description: "This column has a value getter and is not sortable.",
// 		sortable: false,
// 		width: 160,
// 		valueGetter: (params) =>
// 			`${params.getValue(params.id, "firstName") || ""} ${
// 				params.getValue(params.id, "lastName") || ""
// 			}`,
// 	},
// ];

// const rows = [
// 	{ id: 1, lastName: "Snow", firstName: "Jon", age: 35 },
// 	{ id: 2, lastName: "Lannister", firstName: "Cersei", age: 42 },
// 	{ id: 3, lastName: "Lannister", firstName: "Jaime", age: 45 },
// 	{ id: 4, lastName: "Stark", firstName: "Arya", age: 16 },
// 	{ id: 5, lastName: "Targaryen", firstName: "Daenerys", age: null },
// 	{ id: 6, lastName: "Melisandre", firstName: null, age: 150 },
// 	{ id: 7, lastName: "Clifford", firstName: "Ferrara", age: 44 },
// 	{ id: 8, lastName: "Frances", firstName: "Rossini", age: 36 },
// 	{ id: 9, lastName: "Roxie", firstName: "Harvey", age: 65 },
// ];
