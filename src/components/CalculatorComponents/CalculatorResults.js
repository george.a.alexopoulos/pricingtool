import React, { useState, useContext, useEffect, useRef } from "react";
import { SourcesContext } from "../contexts/SourcesContext";
import { useData } from "../contexts/SelectedDataContext";
import { useSelectedServiceFees } from "../contexts/SelectedServiceFeesContext";
import { useSelectedGeneralFees } from "../contexts/SelectedGeneralFeesContext";
import { useSelectedSupplementFees } from "../contexts/SelectedSupplementFeesContext";
import { useSelectedOutlaysFees } from "../contexts/SelectedOutlaysFeesContext";
import { useHistory } from "react-router";
import { Card, CardContent, Typography } from "@material-ui/core";
import { Table, TableCell, TableRow, TableBody } from "@material-ui/core";
import { Grid } from "@material-ui/core";
import { alpha } from "@material-ui/core/styles/";
import MaterialTable, { MTableBody } from "material-table";
import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core/styles";
import { useReactToPrint } from "react-to-print";

const useStyles = makeStyles((theme) => ({
	// root: {
	// 	flexGrow: 1,
	// },
	root: {
		margin: theme.spacing(2, 0, 2),
		// margin: "50px 20px 10px 20px",
		width: "100%",
		textAlign: "center",
		padding: "10px",
		// fontFamily: "Lato",
		fontSize: 12,
		// position: "relative",
	},
	MuiGrid: {
		width: "100%",
	},
	saveButton: {
		margin: "2em 2em 0em 2em",
		height: "40px",
		minWidth: "10rem",
		"&:hover": {
			backgroundColor: alpha(theme.palette.primary.main, 0.5),
		},
	},
	printButton: {
		margin: "0em 0em 1em 0em",
		justifyContent: "cemter",
		textAlign: "cemter",
		height: "40px",
		minWidth: "10rem",
		// width: "165px",
		"&:hover": {
			backgroundColor: alpha(theme.palette.primary.main, 0.5),
		},
	},
	footer: {
		"& > td > div": {
			height: 30,
			minHeight: 30,
		},
		// backgroundColor: "#ffe9bf",
		backgroundColor: "rgba(0, 0, 0, 0.13)",
		color: "black",
		height: 100,
		borderRadius: "25%",
		textAlign: "right",
		alignContent: "right",
	},
	results: {
		"& > td > div": {
			height: 30,
			minHeight: 30,
		},
		// backgroundColor: "#ffe9bf",
		margin: "1em 2em 1em 0em",
		alignSelf: "center",
		backgroundColor: "rgba(0, 0, 0, 0.13)",
		color: "black",
		height: 100,
		width: "100%",
		borderRadius: "12px",
		textAlign: "right",
		alignContent: "right",
	},
}));

const Results = React.forwardRef((props, ref) => {
	// const [selectedRows, setSelectedRows] = useState([]);
	const { data: selectedData } = useData();
	const { sources } = useContext(SourcesContext);
	const { selectedServiceFees } = useSelectedServiceFees();
	const { selectedGeneralFees } = useSelectedGeneralFees();
	const { selectedSupplementFees } = useSelectedSupplementFees();
	const { selectedOutlaysFees } = useSelectedOutlaysFees();
	const selectedSource = selectedData.source;
	// console.log(selectedData.service);
	// console.log(selectedSource);
	// const selectedService = selectedData.service;
	const filteredSource = sources.filter((source) => source.sourceName === selectedSource);
	// console.log(filteredSource[0].sourceName);

	const initialDataRaw = [
		...selectedServiceFees,
		...selectedGeneralFees,
		...selectedSupplementFees,
		...selectedOutlaysFees,
	];

	// function unformatter(value) {
	// 	// console.log(value);
	// 	// value = value.replace("£", "");
	// 	// value = value.replace(",", "");
	// 	value = value.replace(/\D/g, "");
	// 	// console.log(value);

	// 	value = parseFloat(value);
	// 	value = value / 100;

	// 	return value; //returns --> 1200.51
	// }

	const [data, setData] = useState(initialDataRaw);
	const [totalFee, setTotalFee] = useState("");
	const [totalVat, setTotalVat] = useState("");
	const [totalPrice, setTotalPrice] = useState("");

	// console.log("initialDataRaw", initialDataRaw);

	// console.log("data", data);
	// console.log(filteredSource);
	// console.log(selectedData);
	// console.log("Selected Service Fees", selectedServiceFees);
	// console.log("Selected General Fees", selectedGeneralFees);
	// console.log("Selected Supplement Fees", selectedSupplementFees);
	// console.log("Selected Outlay Fees", selectedOutlaysFees);

	// useEffect(() => {
	// 	console.log(data);
	// 	console.log(data[0]);
	// }, [data]);

	const columns2 = [
		{ title: "Id", field: "id", hidden: true, editable: "never" },
		{ title: "Fee Name", field: "feeName", grouping: false, editable: "never" },
		{ title: "Fee Category", field: "feeCategory", defaultGroupOrder: 0, editable: "never" },
		{
			title: "FEE VATABLE",
			field: "feeVatable",
			grouping: false,
			hidden: true,
			editable: "never",
		},
		{ title: "Fee Description", field: "feeDescription", grouping: false, editable: "never" },
		{
			title: "Fee Price",
			field: "feePrice",
			grouping: false,
			type: "numeric",
			validate: (rowData) =>
				rowData.feePrice < 0
					? { isValid: false, helperText: "Price must be above 0" }
					: true,
		},
		{ title: "Vat", field: "feeVat", grouping: false, editable: "never" },
		{
			title: "Fee Total Price",
			field: "feeTotalPrice",
			grouping: false,
			// type: "numeric",
			editable: "never",
		},
	];

	// console.log(columns2);

	useEffect(() => {
		// console.log(data);
		data.map((fee) => {
			// console.log(fee);
			// console.log(fee.feeVatable);
			fee.feeVatable === true
				? (fee.feeVat = new Intl.NumberFormat("en-GB", {
						style: "currency",
						currency: "GBP",
				  }).format(parseFloat((0.2 * parseFloat(fee.feePrice.replace(/\D/g, ""))) / 100)))
				: (fee.feeVat = new Intl.NumberFormat("en-GB", {
						style: "currency",
						currency: "GBP",
				  }).format(0));
			fee.feeVatable === true
				? (fee.feeTotalPrice = new Intl.NumberFormat("en-GB", {
						style: "currency",
						currency: "GBP",
				  }).format(
						parseFloat(fee.feePrice.replace(/\D/g, "")) / 100 +
							parseFloat((0.2 * parseFloat(fee.feePrice.replace(/\D/g, ""))) / 100)
				  ))
				: (fee.feeTotalPrice = new Intl.NumberFormat("en-GB", {
						style: "currency",
						currency: "GBP",
				  }).format(parseFloat(fee.feePrice.replace(/\D/g, "")) / 100));

			// if (fee.feeVatable === true) {
			// 	fee.feeVat = new Intl.NumberFormat("en-GB", {
			// 		style: "currency",
			// 		currency: "GBP",
			// 	}).format(parseFloat((0.2 * parseFloat(fee.feePrice.replace(/\D/g, ""))) / 100));
			// 	fee.feeTotalPrice = new Intl.NumberFormat("en-GB", {
			// 		style: "currency",
			// 		currency: "GBP",
			// 	}).format(
			// 		parseFloat(fee.feePrice.replace(/\D/g, "")) / 100 +
			// 			parseFloat((0.2 * parseFloat(fee.feePrice.replace(/\D/g, ""))) / 100)
			// 	);
			// } else {
			// 	fee.feeVat = new Intl.NumberFormat("en-GB", {
			// 		style: "currency",
			// 		currency: "GBP",
			// 	}).format(0);
			// 	fee.feeTotalPrice = new Intl.NumberFormat("en-GB", {
			// 		style: "currency",
			// 		currency: "GBP",
			// 	}).format(parseFloat(fee.feePrice.replace(/\D/g, "")) / 100);
			// }

			// return;
		});

		const totalFeeNet = data
			.map((fee) => parseFloat(fee.feePrice.replace(/\D/g, "")) / 100)
			// .map((fee) => unformatter(fee.feePrice))
			.reduce((acc, score) => acc + score, 0);
		// console.log(totalFeeNet);
		setTotalFee(totalFeeNet);
		const vatTotal = data
			.filter((fee) => fee.feeVatable)
			// // 	.map((fee) => unformatter(fee.feeTotalPrice) * 0.2)
			.map((fee) => parseFloat((0.2 * parseFloat(fee.feePrice.replace(/\D/g, ""))) / 100))
			.reduce((acc, score) => acc + score, 0);
		// console.log(vatTotal);
		setTotalVat(vatTotal);
		const total = data
			// .map((fee) => unformatter(fee.feeTotalPrice))
			.map(
				(fee) =>
					parseFloat(fee.feePrice.replace(/\D/g, "")) / 100 +
					parseFloat((0.2 * parseFloat(fee.feePrice.replace(/\D/g, ""))) / 100)
			)
			.reduce((acc, score) => acc + score, 0);
		// console.log(total);
		setTotalPrice(total);
	}, [data]);

	const history = useHistory();

	const classes = useStyles();

	return (
		<Card className={classes.root}>
			<CardContent ref={ref}>
				<Typography variant="h2" style={{ margin: "0em 0em 0.5em 0em" }}>
					{`Selected Fees Overview`}
				</Typography>
				{/* <Typography variant="h6" style={{ margin: "1em 0em 0.5em 0em" }}>
					Select fees that apply
				</Typography> */}
				{/* <div style={{ height: 425, width: "100%" }}> */}
				<MaterialTable
					title={`Selected Fees for ${filteredSource[0].sourceName} for ${selectedData.service}`}
					data={data}
					columns={columns2}
					options={{
						search: false,
						paging: false,
						exportButton: true,
						actionsColumnIndex: -1,
						grouping: true,
						defaultExpanded: true,
						rowStyle: {
							backgroundColor: "#EEE",
							textAlign: "end",
							alignContent: "end",
						},
					}}
					editable={{
						onRowUpdate: (newData, oldData) =>
							new Promise((resolve, reject) => {
								setTimeout(() => {
									const dataUpdate = [...data];
									const index = oldData.tableData.id;
									// console.log(newData);
									newData.feeVatable
										? (newData.feeVat = new Intl.NumberFormat("en-GB", {
												style: "currency",
												currency: "GBP",
										  }).format(newData.feePrice * 0.2))
										: (newData.feeVat = new Intl.NumberFormat("en-GB", {
												style: "currency",
												currency: "GBP",
										  }).format(0));
									newData.feeVatable
										? (newData.feeTotalPrice = new Intl.NumberFormat("en-GB", {
												style: "currency",
												currency: "GBP",
										  }).format(newData.feePrice + newData.feePrice * 0.2))
										: (newData.feeTotalPrice = new Intl.NumberFormat("en-GB", {
												style: "currency",
												currency: "GBP",
										  }).format(newData.feePrice));
									// console.log(newData.feeVat);
									// console.log(newData.feeTotalPrice);

									newData.feePrice = new Intl.NumberFormat("en-GB", {
										style: "currency",
										currency: "GBP",
									}).format(newData.feePrice);
									dataUpdate[index] = newData;
									setData(dataUpdate);
									resolve();
								}, 10);
							}),
					}}
					components={{
						Body: (props) => {
							// console.log(props);
							// props.renderData.map(fee =)}
							// const calcFeeTotalPrice = () => props.renderData.map(fee =)}
							return (
								<>
									<MTableBody {...props} />
								</>
							);
						},
					}}
				/>
				<Table className={classes.results}>
					<TableBody>
						<TableRow>
							<TableCell colSpan={5} />
							<TableCell colSpan={5} />
							<TableCell colSpan={5} />
							<TableCell colSpan={5} />
							<TableCell
								colSpan={2}
								style={{
									textAlign: "right",
									fontSize: "large",
									fontWeight: "bold",
								}}
							>{`Total Fee Price (Net of Vat) :`}</TableCell>
							<TableCell
								colSpan={1}
								style={{
									fontSize: "large",
									fontWeight: "bold",
								}}
							>
								{`${new Intl.NumberFormat("en-GB", {
									style: "currency",
									currency: "GBP",
								}).format(totalFee)}`}
							</TableCell>
						</TableRow>
						<TableRow>
							<TableCell colSpan={5} />
							<TableCell colSpan={5} />
							<TableCell colSpan={5} />
							<TableCell colSpan={5} />
							<TableCell
								colSpan={2}
								style={{
									textAlign: "right",
									fontSize: "large",
									fontWeight: "bold",
								}}
							>{`Total Vat : `}</TableCell>
							<TableCell
								colSpan={1}
								style={{
									fontSize: "large",
									fontWeight: "bold",
								}}
							>
								{`${new Intl.NumberFormat("en-GB", {
									style: "currency",
									currency: "GBP",
								}).format(totalVat)}`}
							</TableCell>
						</TableRow>
						<TableRow>
							<TableCell colSpan={5} />
							<TableCell colSpan={5} />
							<TableCell colSpan={5} />
							<TableCell colSpan={5} />
							<TableCell
								colSpan={2}
								style={{
									minWidth: "35em",
									textAlign: "right",
									fontSize: "large",
									fontWeight: "bold",
								}}
							>{`Total Fee Price (incl. Vat) : `}</TableCell>

							<TableCell
								colSpan={1}
								style={{
									fontSize: "large",
									fontWeight: "bold",
								}}
							>
								{`${new Intl.NumberFormat("en-GB", {
									style: "currency",
									currency: "GBP",
								}).format(totalPrice)}`}
							</TableCell>
						</TableRow>
					</TableBody>
				</Table>

				<Button
					variant="contained"
					color="primary"
					className={classes.saveButton}
					onClick={() => {
						history.push("/calculator");
					}}
				>
					START OVER
				</Button>

				{/* <pre style={{ fontSize: 10 }}>{JSON.stringify(selectedRows, null, 4)}</pre> */}
			</CardContent>
		</Card>
	);
});

const CalculatorResults = () => {
	const componentRef = useRef();
	const handlePrint = useReactToPrint({
		content: () => componentRef.current,
	});
	const classes = useStyles();
	return (
		<Card className={classes.root}>
			<CardContent>
				<Grid container direction="column">
					<Grid item>
						<Results ref={componentRef} />
					</Grid>
					<Grid
						container
						direction="column"
						item
						style={{
							alignItems: "center",
							alignSelf: "center",
							justifyContent: "center",
							verticalAlign: "middle",
							// position: "relative",
						}}
					>
						<Grid
							item
							// fullwidth
							style={
								{
									// position: "absolute",
								}
							}
						>
							<Button
								type="button"
								variant="contained"
								color="primary"
								className={classes.printButton}
								onClick={handlePrint}
							>
								Print Fees
							</Button>
						</Grid>
					</Grid>
				</Grid>
			</CardContent>
		</Card>
	);
};

export default CalculatorResults;
