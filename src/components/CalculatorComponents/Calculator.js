import React, { useEffect, useContext } from "react";
import { SourcesContext } from "../contexts/SourcesContext";
import { useData } from "../contexts/SelectedDataContext";
import { useForm } from "react-hook-form";
import { useHistory } from "react-router";
import { Card, CardContent, Typography } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
// import Container from "@material-ui/core/Container";
import TextField from "@material-ui/core/TextField";
// import FormControlLabel from "@material-ui/core/FormControlLabel";
// import Checkbox from "@material-ui/core/Checkbox";
import ReactHookFormSelect from "./formComponents/ReactHookFormSelect";
// import ReactHookFormCheckbox from "./formComponents/ReactHookFormCheckbox";
import CssBaseline from "@material-ui/core/CssBaseline";
import { makeStyles } from "@material-ui/core/styles";
import { alpha } from "@material-ui/core/styles/colorManipulator";
import * as Yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { ErrorMessage } from "@hookform/error-message";

const validationSchema = Yup.object().shape({
	source: Yup.string().required("Please select an Introducer"),
	service: Yup.string().required("Please select a service"),
	conveyancingPrice: Yup.number()
		.typeError("Please enter a valid Property price")
		.required("Please enter a Property price")
		.positive("Property price has to be a positive number!")
		.min(0.01, "Property price has to be greater than zero!"),
});

const useStyles = makeStyles((theme) => ({
	root: {
		margin: "50px 20px 10px 20px",
		width: "100%",
		textAlign: "center",
		padding: "10px",
		fontSize: "40px",
	},
	MuiGrid: {
		width: "100%",
	},
	saveButton: {
		height: "40px",
		minWidth: "10rem",
		"&:hover": {
			backgroundColor: alpha(theme.palette.primary.main, 0.5),
		},
	},
	error: {
		// color: "red",
		color: theme.palette.error.main,
		fontSize: "12px",
		marginTop: "6px",
	},
}));

const services = [
	{
		id: 1,
		value: "Sale Conveyancing",
	},
	{
		id: 2,
		value: "Purchase Conveyancing",
	},
	{
		id: 3,
		value: "Remortgage Conveyancing",
	},
];

const initialValues = {
	source: "",
	service: "",
	conveyancingPrice: "",
};

function Calculator() {
	// const [selectedData, setSelectedData] = useState(initialValues);
	const { setData } = useData();
	// console.log(data);

	const { sources } = useContext(SourcesContext);

	// console.log(sources);

	const sourceOptions = sources.map((source) => ({
		id: source.id,
		value: source.sourceName,
	}));
	// console.log(sourceOptions);

	const sortedSourceOptions = sourceOptions.sort((a, b) =>
		a.value.toLowerCase() > b.value.toLowerCase() ? 1 : -1
	);
	// console.log(sortedSourceOptions);

	const {
		register,
		handleSubmit,
		control,
		reset,
		formState: { errors, isSubmitSuccessful },
	} = useForm({
		mode: "onBlur",
		resolver: yupResolver(validationSchema),
	});

	const history = useHistory();

	useEffect(() => {
		if (isSubmitSuccessful) {
			// console.log(data);
			reset(initialValues);
		}
	}, [isSubmitSuccessful, reset]);

	const onSubmit = (data) => {
		setData(data);
		history.push("/calculator/step2");
	};

	const classes = useStyles();

	return (
		<Card className={classes.root}>
			<CardContent>
				<Typography component="h1" variant="h3">
					Introducer Pricing
				</Typography>
				<CssBaseline />
				<form onSubmit={handleSubmit(onSubmit)}>
					<Grid container direction="column" alignItems="center" spacing={5}>
						<Grid item style={{ width: "15em", margin: "1em 0em 0em " }}>
							<ReactHookFormSelect
								name="source"
								// label="Source"
								label="Introducer"
								control={control}
								defaultValue=""
								rules={{ required: "Introducer is Required" }}
								options={sortedSourceOptions}
								// mapkey="id"
								mapValue="sourceName"
							/>
							<ErrorMessage
								errors={errors}
								name="source"
								render={({ message }) => <p className={classes.error}>{message}</p>}
							/>
						</Grid>
						<Grid item style={{ width: "15em" }}>
							<ReactHookFormSelect
								name="service"
								label="Service"
								control={control}
								defaultValue=""
								rules={{ required: "Service Required" }}
								options={services}
								// error={!!errors.service}
								// helperText={errors?.service?.message}
							/>
							<ErrorMessage
								errors={errors}
								name="service"
								render={({ message }) => <p className={classes.error}>{message}</p>}
							/>
						</Grid>
						<Grid item>
							<TextField
								id="outlined-basic"
								label="Property Price"
								variant="outlined"
								type="number"
								{...register("conveyancingPrice", {
									required: true,
								})}
								error={!!errors.conveyancingPrice}
								// helperText={errors?.conveyancingPrice?.message}
								// inputRef={conveyancingPriceInput.ref}
								// {...inputProps}
							/>
							<ErrorMessage
								errors={errors}
								name="conveyancingPrice"
								render={({ message }) => <p className={classes.error}>{message}</p>}
							/>
						</Grid>

						<Grid item>
							<Button
								// disabled={isValid || formik.isSubmitting}
								// disabled={!isValid}
								variant="contained"
								color="primary"
								className={classes.saveButton}
								type="submit"
								// startIcon={
								// 	formik.isSubmitting ? (
								// 		<CircularProgress size="1rem" />
								// 	) : undefined
								// }
							>
								{"Get Fees"}
							</Button>
						</Grid>
					</Grid>
				</form>
			</CardContent>
		</Card>
	);
}

export default Calculator;

// {
// 	/* <Grid item>
// 							<FormControlLabel
// 								label="Some Fee"
// 								control={
// 									<Controller
// 										control={control}
// 										name="checkedB"
// 										color="primary"
// 										defaultValue={false}
// 										render={({
// 											field: { onChange, onBlur, value, name, ref },
// 											fieldState: { invalid, isTouched, isDirty, error },
// 											formState,
// 										}) => (
// 											<Checkbox
// 												onBlur={onBlur} // notify when input is touched
// 												onChange={onChange} // send value to hook form
// 												checked={value}
// 												inputRef={ref}
// 											/>
// 										)}
// 									/>
// 								}
// 							/>
// 						</Grid> */
// }
// {
// 	/* <Grid item>
// 							<ReactHookFormCheckbox
// 								label="Some other Fee"
// 								name="fee2"
// 								control={control}
// 								color="primary"
// 								defaultValue={false}
// 								control={control}
// 							/>
// 						</Grid> */
// }
