import React, { useState, useContext } from "react";
import { SourcesContext } from "../contexts/SourcesContext";
import { useData } from "../contexts/SelectedDataContext";
import { useSelectedOutlaysFees } from "../contexts/SelectedOutlaysFeesContext";
import { useHistory } from "react-router";
import { Card, CardContent, Typography } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core/styles";
import { alpha } from "@material-ui/core/styles/colorManipulator";

import { DataGrid } from "@material-ui/data-grid";
// import { array } from "yup/lib/locale";

const useStyles = makeStyles((theme) => ({
	// root: {
	// 	flexGrow: 1,
	// },
	root: {
		margin: theme.spacing(3, 0, 2),
		// margin: "50px 20px 10px 20px",
		width: "100%",
		textAlign: "center",
		padding: "10px",
		// fontFamily: "Lato",
		fontSize: 12,
		overfowWrap: "anywhere",
		// position: "relative",
	},
	MuiGrid: {
		width: "100%",
	},
	saveButton: {
		margin: "2em 2em 0em 2em",
		height: "40px",
		minWidth: "10rem",
		"&:hover": {
			backgroundColor: alpha(theme.palette.primary.main, 0.5),
		},
	},
}));

function CalculatorStep5() {
	const [selectedRows, setSelectedRows] = useState([]);
	const { sources } = useContext(SourcesContext);
	const { setSelectedOutlaysFees } = useSelectedOutlaysFees();
	const { data: selectedData } = useData();
	const selectedSource = selectedData.source;
	// const selectedService = selectedData.service;
	const conveyancingPrice = selectedData.conveyancingPrice;
	const filteredSource = sources.filter((source) => source.sourceName === selectedSource);

	// console.log(filteredSource);

	const outlaysFees = filteredSource[0].standardOutlaysFees;
	// console.log(outlaysFees);

	const selectedFeePrice = (fee) => {
		if (fee.feeType === "fixed") {
			return new Intl.NumberFormat("en-GB", {
				style: "currency",
				currency: "GBP",
			}).format(fee.fixedFee.feePrice);
		} else {
			const saleVarFee = filteredSource[0].saleFees[0].variableFee.filter(
				(varFee) =>
					varFee.scaleMin < conveyancingPrice && varFee.scaleMax > conveyancingPrice
			);
			// console.log(saleVarFee);
			return new Intl.NumberFormat("en-GB", {
				style: "currency",
				currency: "GBP",
			}).format(saleVarFee[0].feePrice);
		}
	};

	const rows2 = outlaysFees.map((fee) => {
		// console.log(fee);
		let newFee = {};
		newFee.id = fee.fee_id;
		newFee.feeName = fee.feeName;
		newFee.feePrice = selectedFeePrice(fee);
		newFee.feeCategory = `Outlay Fees`;
		newFee.feeVatable = fee.vatable;
		newFee.feeDescription = fee.feeDescription;
		return newFee;
	});

	// console.log(rows2);

	const columns2 = [
		{ field: "id", headerName: "FEE ID", width: 90, hide: true },
		{
			field: "feeName",
			headerName: "Fee Name",
			width: 275,
			editable: false,
			sortable: false,
		},
		{
			field: "feePrice",
			headerName: "Fee Price",
			width: 100,
			editable: false,
			sortable: false,
		},
		{
			field: "feeCategory",
			headerName: "Fee Category",
			width: 250,
			editable: false,
			sortable: false,
		},
		{ field: "feeVatable", headerName: "FEE VATABLE", width: 90, hide: true },
		{
			field: "feeDescription",
			headerName: "Fee Description",
			width: 490,
			editable: false,
			sortable: false,
			// renderCell: (params) => <div style={{ overflowWrap: "anywhere" }}>{params.value}</div>,
		},
	];

	// console.log(columns2);

	const history = useHistory();

	const classes = useStyles();
	return (
		<Card className={classes.root}>
			<CardContent>
				<Typography variant="h2" style={{ margin: "1em 0em 0.5em 0em" }}>
					{`Outlay Fees`}
				</Typography>
				<Typography variant="h6" style={{ margin: "1em 0em 0.5em 0em" }}>
					Select fees that apply
				</Typography>
				<div style={{ height: 425, width: "100%" }}>
					<DataGrid
						rows={rows2}
						columns={columns2}
						disableColumnMenu
						pageSize={10}
						rowsPerPageOptions={[10]}
						checkboxSelection
						disableSelectionOnClick
						onSelectionModelChange={(ids) => {
							const selectedIDs = new Set(ids);
							const selectedRows = rows2.filter((row) => selectedIDs.has(row.id));
							setSelectedRows(selectedRows);
						}}
					/>
				</div>
				<Button
					variant="contained"
					color="primary"
					className={classes.saveButton}
					onClick={() => {
						history.push("/calculator/step4");
					}}
				>
					BACK
				</Button>
				<Button
					variant="contained"
					color="primary"
					className={classes.saveButton}
					onClick={() => {
						// console.log(selectedRows)
						setSelectedOutlaysFees(selectedRows);
						history.push("/calculator/results");
					}}
				>
					NEXT
				</Button>

				{/* <pre style={{ fontSize: 10 }}>{JSON.stringify(selectedRows, null, 4)}</pre> */}
			</CardContent>
		</Card>
	);
}

export default CalculatorStep5;
