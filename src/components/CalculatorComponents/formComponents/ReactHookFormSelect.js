import React from "react";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";

import { Controller } from "react-hook-form";

const ReactHookFormSelect = ({
	name,
	label,
	control,
	defaultValue,
	rules,
	options,
	children,
	// mapkey,
	mapValue,
	...props
}) => {
	const labelId = `${name}-label`;
	const generateSingleOptions = () => {
		return options.map((option, index) => {
			return (
				<MenuItem key={option.id} value={option.value}>
					{option.value}
				</MenuItem>
			);
		});
	};
	return (
		<FormControl fullWidth variant="outlined">
			<InputLabel htmlFor={labelId}>{`Select ${label}`}</InputLabel>
			<Controller
				name={name}
				control={control}
				rules={rules}
				render={({
					field: { onChange, value },
					fieldState: { invalid, isTouched, isDirty, error },
					formState,
				}) => (
					<Select value={value} onChange={onChange} label={label} labelId={labelId}>
						{generateSingleOptions()}
						{/* <MenuItem value="">select source</MenuItem>
										<MenuItem value="250">$250</MenuItem>
										<MenuItem value="500">$500</MenuItem>
										<MenuItem value="$1000">$1000</MenuItem> */}
					</Select>
				)}
				defaultValue={defaultValue} // make sure to set up defaultValue
			/>
		</FormControl>
	);
};
export default ReactHookFormSelect;
