import React from "react";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
// import FormControl from "@material-ui/core/FormControl";
// import InputLabel from "@material-ui/core/InputLabel";
// import Select from "@material-ui/core/Select";
// import MenuItem from "@material-ui/core/MenuItem";

import { Controller } from "react-hook-form";

const ReactHookFormCheckbox = ({
	control,
	name,
	color,
	label,
	defaultValue,
	rules,
	options,
	children,
	...props
}) => {
	return (
		<FormControlLabel
			label={label}
			control={
				<Controller
					control={control}
					name={name}
					color={color}
					defaultValue={defaultValue}
					render={({
						field: { onChange, onBlur, value, name, ref },
						fieldState: { invalid, isTouched, isDirty, error },
						formState,
					}) => (
						<Checkbox
							onBlur={onBlur} // notify when input is touched
							onChange={onChange} // send value to hook form
							checked={value}
							inputRef={ref}
						/>
					)}
				/>
			}
		/>
	);
};

export default ReactHookFormCheckbox;
