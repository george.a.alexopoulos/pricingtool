import React from "react";
import { Card, CardContent, Typography } from "@material-ui/core";
import useStyles from "./hooks/useStyles";

function Home() {
	const classes = useStyles();

	return (
		<Card className={classes.root}>
			<CardContent>
				<Typography variant="h4">Home</Typography>
				<Typography variant="h4">HOME</Typography>
			</CardContent>
		</Card>
	);
}

export default Home;
