import React from "react";
import SideDrawer from "./components/SideDrawer";
import { CssBaseline } from "@material-ui/core";

function App() {
	return (
		<div>
			<SideDrawer />
			<CssBaseline />
		</div>
	);
}

export default App;
